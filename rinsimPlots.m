% A script that reads csv files and plots results for RinSim simulations
close all;
rerun = false;

if rerun
    
    clear all; clc;
    
    formatSpec = '%d%C%C%d%d%d';
    allCsvFiles = dir(fullfile(pwd,'*.csv')); % gets all csv files in struct

    simulationEndTimes = zeros(5, 5); % Stores simulation data per config
    batteryPickups = zeros(5, 5);
    FertilizerUsages = zeros(5, 5);
    PesticideUsages = zeros(5, 5);
    FertilizerDelegations = zeros(5, 5);
    PesticideDelegations = zeros(5, 5);
    
    simulationEndTimesNoScout = zeros(4);
    batteryPickupsNoScout = zeros(4);
    FertilizerUsagesNoScout = zeros(4);
    PesticideUsagesNoScout = zeros(4);

    for i = 1:length(allCsvFiles)
        fileName = allCsvFiles(i).name;
        T = readtable(fileName,'Delimiter',',', 'Format',formatSpec);

        scouts = table2array(T(1,4));
        bombers = table2array(T(1,5));
        tanks = table2array(T(1,6));

        if scouts == 1

            indicesFertilizer = find(T.event == 'UseFertilizer');
            indicesPesticide = find(T.event == 'UsePesticide');
            indicesBatteryPickups = find(T.event == 'PickUpBattery');
            indicesFertilizerDelegations = find(T.event == 'TankNutrientPoorPlant');
            indicesPesticideDelegations = find(T.event == 'BomberInfestedPlant');

            countBatteryPickups = length(indicesBatteryPickups);
            countFertilizerUsages = length(indicesFertilizer);
            countPesticideUsages = length(indicesPesticide);
            countFertilizerDelegations = length(indicesFertilizerDelegations);
            countPesticideDelegations = length(indicesPesticideDelegations);

            simulationEndTime = max(table2array(T(max(indicesFertilizer), 1)), table2array(T(max(indicesPesticide), 1)));
            simulationEndTimes(bombers, tanks) = simulationEndTime;
            batteryPickups(bombers, tanks) = countBatteryPickups;
            FertilizerUsages(bombers, tanks) = countFertilizerUsages;
            PesticideUsages(bombers, tanks) = countPesticideUsages;
            FertilizerDelegations(bombers, tanks) = countFertilizerDelegations;
            PesticideDelegations(bombers, tanks) = countPesticideDelegations;           
            
        else
            indicesFertilizer = find(T.event == 'UseFertilizer');
            indicesPesticide = find(T.event == 'UsePesticide');
            indicesBatteryPickups = find(T.event == 'PickUpBattery');
            
            countBatteryPickups = length(indicesBatteryPickups);
            countFertilizerUsages = length(indicesFertilizer);
            countPesticideUsages = length(indicesPesticide);
            simulationEndTime = max(table2array(T(max(indicesFertilizer), 1)), table2array(T(max(indicesPesticide), 1)));            
            if bombers == 4
                simulationEndTimesNoScout(1) = simulationEndTime;
                batteryPickupsNoScout(1) = countBatteryPickups;
                FertilizerUsagesNoScout(1)= countFertilizerUsages;
                PesticideUsagesNoScout(1) = countPesticideUsages;
            else
                simulationEndTimesNoScout(2) = simulationEndTime;
                batteryPickupsNoScout(2) = countBatteryPickups;
                FertilizerUsagesNoScout(2)= countFertilizerUsages;
                PesticideUsagesNoScout(2) = countPesticideUsages;
            end            
        end
        
    end
    simulationEndTimesNoScout(3) = simulationEndTimes(4, 4);
    batteryPickupsNoScout(3) = batteryPickups(4, 4);
    FertilizerUsagesNoScout(3)= FertilizerUsages(4, 4);
    PesticideUsagesNoScout(3) = PesticideUsages(4, 4);

    simulationEndTimesNoScout(4) = simulationEndTimes(5, 5);
    batteryPickupsNoScout(4) = batteryPickups(5, 5);
    FertilizerUsagesNoScout(4)= FertilizerUsages(5, 5);
    PesticideUsagesNoScout(4) = PesticideUsages(5, 5);
        
end

showSurfacePlots = false;
show2dPlots = true;
maxBombers = 5;
maxTanks = 5;

if show2dPlots
    figure
    b = bar([simulationEndTimesNoScout(1) simulationEndTimesNoScout(3); simulationEndTimesNoScout(2) simulationEndTimesNoScout(4)],'FaceColor','flat');
    title('Simulation Time (with/without Scout)')
    xticklabels({'Bombers:4 Tanks:4','Bombers:5 Tanks:5'})
    legend('Without Scout','With Scout','Location','northwest')
    xlabel('Configuration')
    ylabel('Total Simulation Time')   
    
    figure
    bar(batteryPickupsNoScout)
    b = bar([batteryPickupsNoScout(1) batteryPickupsNoScout(3); batteryPickupsNoScout(2) batteryPickupsNoScout(4)],'FaceColor','flat');
    title('Battery Pickups (with/without Scout)')
    xticklabels({'Bombers:4 Tanks:4','Bombers:5 Tanks:5'})
    legend('Without Scout','With Scout')
    xlabel('Configuration')
    ylabel('Battery Pickups')   
    
    figure    
    bar(FertilizerUsagesNoScout)
    b = bar([FertilizerUsagesNoScout(1) FertilizerUsagesNoScout(3); FertilizerUsagesNoScout(2) FertilizerUsagesNoScout(4)],'FaceColor','flat');
    title('Fertilizer Usages - 5 targets (with/without Scout)')
    xticklabels({'Bombers:4 Tanks:4','Bombers:5 Tanks:5'})
    legend('Without Scout','With Scout', 'Location','southeast')
    xlabel('Configuration')
    ylabel('Fertilizer Usages')       
    
    figure
    bar(PesticideUsagesNoScout)
    b = bar([PesticideUsagesNoScout(1) PesticideUsagesNoScout(3); PesticideUsagesNoScout(2) PesticideUsagesNoScout(4)],'FaceColor','flat');
    title('Pesticide Usages - 6 targets (with/without Scout)')
    xticklabels({'Bombers:4 Tanks:4','Bombers:5 Tanks:5'})
    legend('Without Scout','With Scout', 'Location','southeast')
    xlabel('Configuration')
    ylabel('Pesticide Usages')             
end

if showSurfacePlots

    figure
    [X,Y] = meshgrid(1:maxBombers, 1:maxTanks);
    surf(X,Y,simulationEndTimes)
    title('Simulation Time vs Bombers vs Tanks: MAS HTN (with Scout)')
    xlabel('Tanks')
    ylabel('Bombers')
    zlabel('Total Simulation Time')
    colorbar


    figure
    [X,Y] = meshgrid(1:maxBombers, 1:maxTanks);
    surf(X,Y,batteryPickups)
    title('Battery Pickups vs Bombers vs Tanks: MAS HTN (with Scout)')
    xlabel('Tanks')
    ylabel('Bombers')
    zlabel('Battery Pickups')
    colorbar

    figure
    [X,Y] = meshgrid(1:maxBombers, 1:maxTanks);
    surf(X,Y,FertilizerUsages)
    title('Fertilizer Usages vs Bombers vs Tanks: MAS HTN (with Scout)')
    xlabel('Tanks')
    ylabel('Bombers')
    zlabel('Fertilizer Usages')
    colorbar

    figure
    [X,Y] = meshgrid(1:maxBombers, 1:maxTanks);
    surf(X,Y,PesticideUsages)
    title('Pesticide Usages vs Bombers vs Tanks: MAS HTN (with Scout)')
    xlabel('Tanks')
    ylabel('Bombers')
    zlabel('Pesticide Usages')
    colorbar

    figure
    [X,Y] = meshgrid(1:maxBombers, 1:maxTanks);
    surf(X,Y,FertilizerDelegations)
    title('Fertilizer Delegations vs Bombers vs Tanks: MAS HTN (with Scout)')
    xlabel('Tanks')
    ylabel('Bombers')
    zlabel('Fertilizer Delegations')
    colorbar

    figure
    [X,Y] = meshgrid(1:maxBombers, 1:maxTanks);
    surf(X,Y,PesticideDelegations)
    title('Pesticide Delegations vs Bombers vs Tanks: MAS HTN (with Scout)')
    xlabel('Tanks')
    ylabel('Bombers')
    zlabel('Pesticide Delegations')
    colorbar
end