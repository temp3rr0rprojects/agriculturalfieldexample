/*
 * Copyright (C) 2011-2018 Rinde R.S. van Lon
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package AgriculturalField;

import AgriculturalField.Entities.*;
import AgriculturalField.HierarchicalTaskNetwork.Planner.HTNGenerator;
import AgriculturalField.HierarchicalTaskNetwork.Planner.WorldState;
import com.github.rinde.rinsim.core.Simulator;
import com.github.rinde.rinsim.core.model.comm.CommModel;
import com.github.rinde.rinsim.core.model.road.CollisionPlaneRoadModel;
import com.github.rinde.rinsim.core.model.road.RoadModelBuilders;
import com.github.rinde.rinsim.core.model.time.TimeModel;
import com.github.rinde.rinsim.geom.Point;
import com.github.rinde.rinsim.ui.View;
import com.github.rinde.rinsim.ui.renderers.CommRenderer;
import com.github.rinde.rinsim.ui.renderers.PlaneRoadModelRenderer;
import org.jetbrains.annotations.NotNull;

import javax.measure.unit.SI;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Example showcasing the {@link CollisionPlaneRoadModel}.
 * @author Hoang Tung Dinh
 * @author Rinde van Lon
 */
public final class AgriculturalFieldExample {
  static final double PLANE_SIZE = 2000; // 500 // Tomato Field: 2 km x 2 km (4 km^2)
  static final Point MIN_POINT = new Point(0, 0);
  static final Point MAX_POINT = new Point(PLANE_SIZE, PLANE_SIZE);

  static final long TICK_LENGTH = 20; // in ms
  static final int TESTING_SPEEDUP = 100000; // 10000
  static final long TESTING_END_TIME = 180000L;
  static final long RANDOM_SEED = 66;

  static final double VEHICLE_LENGTH = 1.5; // Dimensions: Scout 1.1m wing span, Bomber 1.5m x 1.5m x 0.5m, Tank 1.5m x 1.4m x 0.8m
  static final double MAX_BOMBER_SPEED = UavAgent.getMetersPerSecond(80); // Bomber MAX SPEED 80km/h
  static final double MAX_SCOUT_SPEED = UavAgent.getMetersPerSecond(110); // Scout MAX SPEED: 110km/h
  static final double MAX_TANK_SPEED = UavAgent.getMetersPerSecond(18); // Tank MAX SPEED 18km/h
  static final double MAX_SPEED = Math.max(Math.max(MAX_BOMBER_SPEED, MAX_SCOUT_SPEED), MAX_TANK_SPEED);

  static final int NUM_BOMBER_UAVS = 5; // Max 5
  static final int NUM_TANK_AGVS = 5; // Max 5
  static final int NUM_SCOUT_UAVS = 1; // Max 1

  static WorldFacts worldFacts;

  private AgriculturalFieldExample() {}

  /**
   * @param args No args.
   */
  public static void main(String[] args) {
    run(false);
  }

  /**
   * Runs the example.
   * @param testing If <code>true</code> the example will run in testing mode,
   *          automatically starting and stopping itself such that it can be run
   *          from a unit test.
   */
  public static void run(boolean testing) {

    worldFacts = new WorldFacts(PLANE_SIZE); // Init world facts
      View.Builder viewBuilder = View.builder()
              .with(PlaneRoadModelRenderer.builder())
              .with(AgriculturalFieldRenderer.builder()
                      .withDifferentColors()
                      .withDestinationLines()
                      .withName())
                .with(CommRenderer.builder()
                      .withReliabilityColors()
                      .withToString()
                      .withMessageCount())
              .withAutoPlay()
              .withTitleAppendix("Agricultural Field");

    if (testing) {
      viewBuilder = viewBuilder.withAutoPlay()
              .withAutoClose()
              .withSimulatorEndTime(TESTING_END_TIME)
              .withTitleAppendix("TESTING Agricultural Field")
              .withSpeedUp(TESTING_SPEEDUP);
    }

      final Simulator sim =
              Simulator.builder()
                      .setRandomSeed(RANDOM_SEED)
                      .addModel(TimeModel.builder().withTickLength(TICK_LENGTH))
                      .addModel(
                              RoadModelBuilders.plane()
                                      .withCollisionAvoidance()
                                      .withObjectRadius(VEHICLE_LENGTH)
                                      .withMinPoint(MIN_POINT)
                                      .withMaxPoint(MAX_POINT)
                                      .withDistanceUnit(SI.METER)
                                      .withSpeedUnit(SI.METERS_PER_SECOND)
                                      .withMaxSpeed(MAX_SPEED))
                      .addModel(CommModel.builder())
                      .addModel(viewBuilder)
                      .build();

    final CollisionPlaneRoadModel model =
      sim.getModelProvider().getModel(CollisionPlaneRoadModel.class);

    HTNGenerator htnGenerator = new HTNGenerator();

    // Add Scout UAVs
    int counter = 0;
    WorldState initScoutUavWorldState = htnGenerator.getInitScoutUavWorldState();
    List<String> scoutUavDesires = Arrays.asList("ChangeBattery");
    for (int i = 0; i < NUM_SCOUT_UAVS; i++) {
      Point initPosition = getPointAround(sim, model, 0.1, 0.2);
      sim.register(new UavAgent(initPosition, "Scout" + Integer.toString(counter++), MAX_SCOUT_SPEED, UavType.Scout, worldFacts, initScoutUavWorldState, scoutUavDesires));
    }

    // Add Bomber UAVs
    counter = 0;
    WorldState initBomberUavWorldState = htnGenerator.getInitBomberUavWorldState();
    List<String> bomberUavDesires = Arrays.asList("SprayInfestedPlant", "ChangeBattery");
    for (int i = 0; i < NUM_BOMBER_UAVS; i++) {
      Point initPosition = getPointAround(sim, model, 0.9, 0.2);
      sim.register(new UavAgent(initPosition, "Bomber" + Integer.toString(counter++), MAX_BOMBER_SPEED, UavType.Bomber, worldFacts, initBomberUavWorldState, bomberUavDesires));
    }

    // Add Tank AGVs
    counter = 0;
    WorldState initAgvWorldState = htnGenerator.getInitTankAgvWorldState();
    List<String> tankAgvDesires = Arrays.asList("FertilizeNutrientPoorPlant", "ChangeBattery");
    for (int i = 0; i < NUM_TANK_AGVS; i++) {
      Point initPosition = getPointAround(sim, model, 0.5, 0.2);
      sim.register(new AgvAgent(initPosition, "Tank" + Integer.toString(counter++), MAX_TANK_SPEED, worldFacts, initAgvWorldState, tankAgvDesires));
    }

    // Store count of agents for logging
    worldFacts.logger = new Logger("simulation_" + getCurrentTimeStamp(), NUM_SCOUT_UAVS, NUM_BOMBER_UAVS, NUM_TANK_AGVS);

    sim.start();
  }

  public static String getCurrentTimeStamp() {
    return new SimpleDateFormat("HH_mm_ss_SSS").format(new Date());
  }

  @NotNull
  private static Point getPointAround(Simulator sim, CollisionPlaneRoadModel model, double xPercent, double yPercent) {
    Point initPosition;
    do {
      Point newPosition = model.getRandomUnoccupiedPosition(sim.getRandomGenerator());
      initPosition = new Point(xPercent * PLANE_SIZE + 0.1 * newPosition.x, yPercent * newPosition.y);
    } while (model.isOccupied(initPosition));
    return initPosition;
  }
}