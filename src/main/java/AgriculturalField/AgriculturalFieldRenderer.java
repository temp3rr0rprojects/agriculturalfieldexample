/*
 * Copyright (C) 2011-2018 Rinde R.S. van Lon
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package AgriculturalField;

import AgriculturalField.Entities.*;
import com.github.rinde.rinsim.core.model.DependencyProvider;
import com.github.rinde.rinsim.core.model.ModelBuilder.AbstractModelBuilder;
import com.github.rinde.rinsim.core.model.road.CollisionPlaneRoadModel;
import com.github.rinde.rinsim.core.model.road.RoadUser;
import com.github.rinde.rinsim.geom.Point;
import com.github.rinde.rinsim.ui.renderers.CanvasRenderer.AbstractCanvasRenderer;
import com.github.rinde.rinsim.ui.renderers.ViewPort;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterators;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.GC;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

final class AgriculturalFieldRenderer extends AbstractCanvasRenderer {
  private static final int FONT_SIZE = 10;

  private final CollisionPlaneRoadModel rm;
  private final Color red;
  private final Color black;
  private final Color green;
  private final Color darkGray;
  private final Color white;
  private final Font labelFont;
  private final ImmutableSet<Opts> vizOptions;
  private final Iterator<Integer> colors = Iterators.cycle(SWT.COLOR_BLUE,
    SWT.COLOR_RED, SWT.COLOR_GREEN, SWT.COLOR_CYAN, SWT.COLOR_MAGENTA,
    SWT.COLOR_YELLOW, SWT.COLOR_DARK_BLUE, SWT.COLOR_DARK_RED,
    SWT.COLOR_DARK_GREEN, SWT.COLOR_DARK_CYAN, SWT.COLOR_DARK_MAGENTA,
    SWT.COLOR_DARK_YELLOW);

  private final Map<RoadUser, Color> colorMap;

  AgriculturalFieldRenderer(CollisionPlaneRoadModel r, Device d, ImmutableSet<Opts> opts) {
    rm = r;
    red = d.getSystemColor(SWT.COLOR_RED);
    black = d.getSystemColor(SWT.COLOR_BLACK);
    darkGray = d.getSystemColor(SWT.COLOR_DARK_GRAY);
    green = d.getSystemColor(SWT.COLOR_GREEN);
    white = d.getSystemColor(SWT.COLOR_WHITE);
    labelFont = new Font(d, "arial", FONT_SIZE, SWT.NORMAL);
    vizOptions = opts;
    colorMap = new LinkedHashMap<>();
  }

  @Override
  public void renderDynamic(GC gc, ViewPort vp, long time) {
    final int radius = 2;
    boolean renderedStationsAndTargets = false;
    final Map<RoadUser, Point> objects = rm.getObjectsAndPositions();
    synchronized (objects) {
      for (final Entry<RoadUser, Point> entry : objects.entrySet()) {
        final Point p = entry.getValue();

        final String name;
        final Point dest;
        final double r = rm.getObjectRadius();
        Color c = red;

        if (!renderedStationsAndTargets) {
            if (entry.getKey() instanceof UavAgent) {

                WorldFacts worldFacts = ((UavAgent) entry.getKey()).worldFacts;
                int magnifyFactor = 100;

                // Stations
                BatteryStation batteryStation = worldFacts.batteryStation;
                FertilizerStation fertilizerStation = worldFacts.fertilizerStation;
                PesticideStation pesticideStation = worldFacts.pesticideStation;

                // Targets
                List<NutrientPoorPlant> nutrientPoorPlantList = worldFacts.nutrientPoorPlantList;
                List<InfestedPlant> infestedPlantList = worldFacts.infestedPlantList;

                // Draw stations
                gc.setBackground(darkGray);
                gc.drawOval(
                        vp.toCoordX(batteryStation.position.x) - vp.scale(batteryStation.radius), vp.toCoordY(batteryStation.position.y) - vp.scale(batteryStation.radius),
                        magnifyFactor * vp.scale(batteryStation.radius), magnifyFactor * vp.scale(batteryStation.radius));
                gc.fillOval(
                        vp.toCoordX(batteryStation.position.x) - vp.scale(batteryStation.radius), vp.toCoordY(batteryStation.position.y) - vp.scale(batteryStation.radius),
                        magnifyFactor * vp.scale(batteryStation.radius), magnifyFactor * vp.scale(batteryStation.radius));
                gc.setBackground(gc.getDevice().getSystemColor(SWT.COLOR_RED));
                gc.drawOval(
                        vp.toCoordX(fertilizerStation.position.x) - vp.scale(fertilizerStation.radius), vp.toCoordY(fertilizerStation.position.y) - vp.scale(fertilizerStation.radius),
                        magnifyFactor * vp.scale(fertilizerStation.radius), magnifyFactor * vp.scale(fertilizerStation.radius));
                gc.fillOval(
                        vp.toCoordX(fertilizerStation.position.x) - vp.scale(fertilizerStation.radius), vp.toCoordY(fertilizerStation.position.y) - vp.scale(fertilizerStation.radius),
                        magnifyFactor * vp.scale(fertilizerStation.radius), magnifyFactor * vp.scale(fertilizerStation.radius));
                gc.setBackground(gc.getDevice().getSystemColor(SWT.COLOR_BLUE));
                gc.drawOval(
                        vp.toCoordX(pesticideStation.position.x) - vp.scale(pesticideStation.radius), vp.toCoordY(pesticideStation.position.y) - vp.scale(pesticideStation.radius),
                        magnifyFactor * vp.scale(pesticideStation.radius), magnifyFactor * vp.scale(pesticideStation.radius));
                gc.fillOval(
                        vp.toCoordX(pesticideStation.position.x) - vp.scale(pesticideStation.radius), vp.toCoordY(pesticideStation.position.y) - vp.scale(pesticideStation.radius),
                        magnifyFactor * vp.scale(pesticideStation.radius), magnifyFactor * vp.scale(pesticideStation.radius));
                gc.drawText("Batteries", vp.toCoordX(batteryStation.position.x), vp.toCoordY(batteryStation.position.y), true);
                gc.drawText("Fertilizer", vp.toCoordX(fertilizerStation.position.x), vp.toCoordY(fertilizerStation.position.y), true);
                gc.drawText("Pesticide", vp.toCoordX(pesticideStation.position.x), vp.toCoordY(pesticideStation.position.y), true);

                // Draw targets
                for(NutrientPoorPlant nutrientPoorPlant : nutrientPoorPlantList) {

                    gc.setBackground(gc.getDevice().getSystemColor(SWT.COLOR_RED));
                    gc.drawOval(
                            vp.toCoordX(nutrientPoorPlant.position.x) - vp.scale(nutrientPoorPlant.radius), vp.toCoordY(nutrientPoorPlant.position.y) - vp.scale(nutrientPoorPlant.radius),
                            magnifyFactor * vp.scale(nutrientPoorPlant.radius), magnifyFactor * vp.scale(nutrientPoorPlant.radius));
                    gc.fillOval(
                            vp.toCoordX(nutrientPoorPlant.position.x) - vp.scale(nutrientPoorPlant.radius), vp.toCoordY(nutrientPoorPlant.position.y) - vp.scale(nutrientPoorPlant.radius),
                            magnifyFactor * vp.scale(nutrientPoorPlant.radius), magnifyFactor * vp.scale(nutrientPoorPlant.radius));
                    if (nutrientPoorPlant.isFertilized)
                        gc.drawText("PlantFertilizedOK", vp.toCoordX(nutrientPoorPlant.position.x), vp.toCoordY(nutrientPoorPlant.position.y), true);
                    else
                        gc.drawText("NutrientPoorPlant", vp.toCoordX(nutrientPoorPlant.position.x), vp.toCoordY(nutrientPoorPlant.position.y), true);

                }

                for(InfestedPlant infestedPlant : infestedPlantList) {
                    gc.setBackground(gc.getDevice().getSystemColor(SWT.COLOR_BLUE));
                    gc.drawOval(
                            vp.toCoordX(infestedPlant.position.x) - vp.scale(infestedPlant.radius), vp.toCoordY(infestedPlant.position.y) - vp.scale(infestedPlant.radius),
                            magnifyFactor * vp.scale(infestedPlant.radius), magnifyFactor * vp.scale(infestedPlant.radius));
                    gc.fillOval(
                            vp.toCoordX(infestedPlant.position.x) - vp.scale(infestedPlant.radius), vp.toCoordY(infestedPlant.position.y) - vp.scale(infestedPlant.radius),
                            magnifyFactor * vp.scale(infestedPlant.radius), magnifyFactor * vp.scale(infestedPlant.radius));
                    if (!infestedPlant.isInfested)
                        gc.drawText("PlantSprayedOK", vp.toCoordX(infestedPlant.position.x), vp.toCoordY(infestedPlant.position.y), true);
                    else
                        gc.drawText("InfestedPlant", vp.toCoordX(infestedPlant.position.x), vp.toCoordY(infestedPlant.position.y), true);
                }

                renderedStationsAndTargets = true;
            }
        }

        // Set name and destination, depending on the agent type
        if (entry.getKey() instanceof UavAgent) {
          UavAgent a = (UavAgent) entry.getKey();
          name = a.getName();
          dest = a.getDestination().orNull();

          if (vizOptions.contains(Opts.DIFFERENT_COLORS)) {
            if (!colorMap.containsKey(a)) {
              colorMap.put(a, gc.getDevice().getSystemColor(colors.next()));
            }
            c = colorMap.get(a);
          }
        } else {
          AgvAgent a = (AgvAgent) entry.getKey();
          name = a.getName();
          dest = a.getDestination().orNull();

          if (vizOptions.contains(Opts.DIFFERENT_COLORS)) {
            if (!colorMap.containsKey(a)) {
              colorMap.put(a, gc.getDevice().getSystemColor(colors.next()));
            }
            c = colorMap.get(a);
          }
        }

        final int xpx = vp.toCoordX(p.x);
        final int ypx = vp.toCoordY(p.y);

        if (vizOptions.contains(Opts.DESTINATION)) {
          if (dest != null) {
            gc.setForeground(c);
            gc.setLineStyle(SWT.LINE_DOT);
            gc.drawLine(vp.toCoordX(dest.x), vp.toCoordY(dest.y), xpx, ypx);
            gc.setLineStyle(SWT.LINE_SOLID);
          }
        }

        if (entry.getKey() instanceof UavAgent) { // If UAV
          if (((UavAgent) entry.getKey()).getType() == UavType.Bomber) {// Render Bomber Multi-rotor
              gc.setBackground(white);
              gc.fillOval(
                      xpx - vp.scale(r), ypx - vp.scale(r),
                      vp.scale(r), vp.scale(r));
              gc.fillOval(
                      xpx , ypx - vp.scale(r),
                      vp.scale(r), vp.scale(r));
              gc.fillOval(
                      xpx, ypx,
                      vp.scale(r), vp.scale(r));
              gc.fillOval(
                      xpx - vp.scale(r), ypx,
                      vp.scale(r), vp.scale(r));

              gc.setForeground(black);
              gc.drawOval(
                      xpx - vp.scale(r), ypx - vp.scale(r),
                      vp.scale(r), vp.scale(r));
              gc.drawOval(
                      xpx , ypx - vp.scale(r),
                      vp.scale(r), vp.scale(r));
              gc.drawOval(
                      xpx, ypx,
                      vp.scale(r), vp.scale(r));
              gc.drawOval(
                      xpx - vp.scale(r), ypx,
                      vp.scale(r), vp.scale(r));

              gc.drawLine((int) (xpx - 0.5 * vp.scale(r)), (int) (ypx - 0.5 * vp.scale(r)), (int) (xpx + 0.5 * vp.scale(r)), (int) (ypx + 0.5 * vp.scale(r)));
              gc.drawLine((int) (xpx - 0.5 * vp.scale(r)), (int) (ypx + 0.5 * vp.scale(r)), (int) (xpx + 0.5 * vp.scale(r)), (int) (ypx - 0.5 * vp.scale(r)));
          } else { // Else render Scout airplane
              int[] wingPlaneShapePoints = new int[] {
                      xpx, ypx - (int)(0.5 * vp.scale(r)),
                      xpx + vp.scale(r), ypx +  vp.scale(r),
                      xpx - vp.scale(r), ypx +  vp.scale(r)};
              gc.setBackground(darkGray);
              gc.fillPolygon(wingPlaneShapePoints);
              gc.setForeground(black);
              gc.drawPolygon(wingPlaneShapePoints);
          }

        } if (entry.getKey() instanceof AgvAgent) { // If AGV
          gc.setBackground(green);
          gc.fillRoundRectangle(
                  xpx - vp.scale(r), ypx - vp.scale(r),
                  2 * vp.scale(r), 2 * vp.scale(r), 15, 15);

          gc.setForeground(black);
          gc.drawRoundRectangle(
                  xpx - vp.scale(r), ypx - vp.scale(r),
                  2 * vp.scale(r), 2 * vp.scale(r), 15, 15);
        }
      }
    }
  }

  @Override
  public void renderStatic(GC gc, ViewPort vp) {}

  static Builder builder() {
    return Builder.create();
  }

  enum Opts {
    DESTINATION, NAME, DIFFERENT_COLORS
  }

  //@AutoValue // TODO: temp disable
  abstract static class Builder
      extends AbstractModelBuilder<AgriculturalFieldRenderer, Void> {
    private static final long serialVersionUID = 701437750634453331L;

    Builder() {
      setDependencies(CollisionPlaneRoadModel.class, Device.class);
    }

    abstract ImmutableSet<Opts> vizOptions();

    public Builder withDestinationLines() {
      return create(Opts.DESTINATION, vizOptions());
    }

    public Builder withName() {
      return create(Opts.NAME, vizOptions());
    }

    public Builder withDifferentColors() {
      return create(Opts.DIFFERENT_COLORS, vizOptions());
    }

    @Override
    public AgriculturalFieldRenderer build(DependencyProvider dependencyProvider) {
      return new AgriculturalFieldRenderer(
        dependencyProvider.get(CollisionPlaneRoadModel.class),
        dependencyProvider.get(Device.class),
        vizOptions());
    }

    static Builder create(Opts opt, ImmutableSet<Opts> opts) {
      return new AutoValue_AgriculturalFieldRenderer_Builder(
        ImmutableSet.<Opts>builder().addAll(opts).add(opt).build());
    }

    static Builder create() {
      return new AutoValue_AgriculturalFieldRenderer_Builder(ImmutableSet.<Opts>of());
    }
  }
}
