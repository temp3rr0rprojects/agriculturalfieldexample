
package AgriculturalField;

import com.google.common.collect.ImmutableSet;

import javax.annotation.Generated;

@Generated("com.google.auto.value.processor.AutoValueProcessor")
 final class AutoValue_AgriculturalFieldRenderer_Builder extends AgriculturalFieldRenderer.Builder {

  private final ImmutableSet<AgriculturalFieldRenderer.Opts> vizOptions;

  AutoValue_AgriculturalFieldRenderer_Builder(
      ImmutableSet<AgriculturalFieldRenderer.Opts> vizOptions) {
    if (vizOptions == null) {
      throw new NullPointerException("Null vizOptions");
    }
    this.vizOptions = vizOptions;
  }

  @Override
  ImmutableSet<AgriculturalFieldRenderer.Opts> vizOptions() {
    return vizOptions;
  }

  @Override
  public boolean equals(Object o) {
    if (o == this) {
      return true;
    }
    if (o instanceof AgriculturalFieldRenderer.Builder) {
      AgriculturalFieldRenderer.Builder that = (AgriculturalFieldRenderer.Builder) o;
      return (this.vizOptions.equals(that.vizOptions()));
    }
    return false;
  }

  @Override
  public int hashCode() {
    int h = 1;
    h *= 1000003;
    h ^= this.vizOptions.hashCode();
    return h;
  }

  private static final long serialVersionUID = 701437750634453331L;

}
