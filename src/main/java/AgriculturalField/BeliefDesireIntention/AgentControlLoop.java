package AgriculturalField.BeliefDesireIntention;

import java.util.List;

public class AgentControlLoop {

    public static boolean succeeded(List<Intention> inputIntensionList, List<Belief> inputBeliefList) {
        boolean isSucceeded = false;

        // TODO: Do stuff succeeded(I, B);

        return isSucceeded;
    }

    public static boolean impossible(List<Intention> inputIntensionList, List<Belief> inputBeliefList) {
        boolean isImpossible = true;

        // TODO: Do stuff impossible(I, B);

        return isImpossible;
    }

    public static void execute(Plan pi) {
        // TODO: execute(pi);
    }

    public static void execute(Action alpha) {
        // TODO: execute(alpha);
    }

    public static boolean reconsider(List<Intention> inputIntensionList, List<Belief> inputBeliefList) {
        boolean doReconsider = true;

        // TODO: Do stuff impossible(I, B);

        return doReconsider;
    }

    public static Percept getNextPercept() {
        Percept rho = new Percept();
        return rho;
    }

    /***
     * Naive BDI version 3: Naive Option-Filter deliberation.
     */
    public static void loopNaiveOptionFilterDeliberation() {
//        List<Belief> initBeliefs = new ArrayList<>();
//        List<Intention> initIntentions = new ArrayList<>();
//
//        List<Belief> beliefs = initBeliefs;
//        List<Intention> intentions = initIntentions;
//
//        int testPerceptTicks = 10;
//        for (int i = 0; i < testPerceptTicks; i++)
//            naiveOptionFilterDeliberation(beliefs, intentions);
    }

//    public static void loopMetaLevelCommitmentStrategy () {
//        List<Belief> initBeliefs = new ArrayList<>();
//        List<Intention> initIntentions = new ArrayList<>();
//
//        List<Belief> beliefs = initBeliefs;
//        List<Intention> intentions = initIntentions;
//
//        int testPerceptTicks = 10;
//        for (int i = 0; i < testPerceptTicks; i++)
//            metaLevelCommitmentStrategy(beliefs, intentions);
//    }

    /***
     * BDI version 7: Meta-level commitment strategy.
     * @param beliefs
     * @param intentions
     */
//    public static void metaLevelCommitmentStrategy(List<Belief> beliefs, List<Intention> intentions) {
//
//        // TODO: HTN-BDI
//        // TODO: Intentions -> Filtered HTN (Top) level Compound tasks -> Filtered Domain
//        // TODO: New percept -> Update on HTN WorldState
//        // TODO: plan(pi) -> new HTNPlanner with updated WorldState, filtered Domain (only chosen compound & primitive tasks)
//
//        // TODO: MAS-HTN
//        // TODO: First abstract HTN plan checks
//        // TODO: If conflict -> flattened HTN plan checks
//        // TODO: Possible Conflicts:
//        // TODO:    - PickUpBattery, PickUpPesticide, PickUpFertilizer: Avoid Concurrent
//        // TODO:    - NavigateToInfestedPlant, NavigateToNutrientPoorPlant, UsePesticide, UseFertilizer: should be different coordinates +/- 1.5m (vehicle radius)
//        // TODO:    - BroadcastInfestedPlantPoint, BroadcastNutrientPoorPlantPoint: Avoid bcast same point (ONLY with >1 Scout UAVs)
//        // TODO:    - (Optional) NavigateToBattery, NavigateToPesticide, NavigateToFertilizer: Avoid Concurrent
//        // TODO:    - (Optional) World_state['InfestedPlants', 'NutrientPoorPlants']: Total counts may be less than reality
//        // TODO: Potential conflict resolution:
//        // TODO:    - Remove (only once) the duplicate concurrent compound task
//        // TODO:    - Wait (trivial time for sim wall time, ~ ms - seconds)
//
//        Percept rho = getNextPercept(); // Sensor stimuli
//        beliefs = Belief.beliefRevisionFunction(beliefs, rho); // B:= brf(B, rho);
//        // Deliberation
//        List<Desire> desires = Desire.options(beliefs, intentions); // D:= options(B, I);
//        intentions = Intention.filter(beliefs, desires, intentions); // I:= filter(B, D, I);
//        Plan pi = new Plan();
//        pi.actionList = Plan.plan(beliefs, intentions); // pi:= plan(B, I);
//
//        Action alpha;
//        while (!(pi.isEmpty() || succeeded(intentions, beliefs) || impossible(intentions, beliefs))){ // while not (empty(pi) or succeeded(I, B) or impossible(I, B) do
//            alpha = Plan.hd(pi.actionList); // alpha:= hd(pi);
//            execute(alpha);
//            pi.actionList = Plan.tail(pi.actionList); // pi:= tail(pi);
//            rho = getNextPercept();
//            beliefs = Belief.beliefRevisionFunction(beliefs, rho); // B:= brf(B, rho);
//            if (reconsider(intentions, beliefs)) { // TODO: Separate strategy i.e 5 min counter. Dynamic environment.
//                // Deliberation
//                desires = Desire.options(beliefs, intentions);
//                intentions = Intention.filter(beliefs, desires, intentions);
//            }
//            if (!Plan.sound(pi.actionList, intentions, beliefs)) {
//                pi.actionList = Plan.plan(beliefs, intentions); // pi:= plan(B, I);
//            }
//        }
//    }

//    public static void naiveOptionFilterDeliberation(List<Belief> beliefs, List<Intention> intentions) {
//        Percept rho = getNextPercept();
//        beliefs = Belief.beliefRevisionFunction(beliefs, rho); // B:= brf(B, rho);
//        // Deliberation
//        List<Desire> desires = Desire.options(beliefs, intentions); // D:= options(B, I);
//        intentions = Intention.filter(beliefs, desires, intentions); // I:= filter(B, D, I);
//        Plan pi = new Plan();
//        pi.actionList = Plan.plan(beliefs, intentions); // pi := plan(B, I);
//        execute(pi);
//    }
}
