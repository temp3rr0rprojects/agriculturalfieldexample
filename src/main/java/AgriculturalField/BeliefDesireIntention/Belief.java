package AgriculturalField.BeliefDesireIntention;

import AgriculturalField.HierarchicalTaskNetwork.Planner.WorldState;
import javafx.util.Pair;

public class Belief {

    public static WorldState beliefRevisionFunction(WorldState inputBeliefs, Pair inputPercept) {
        WorldState returningBeliefs = inputBeliefs;

        String[] parts;
        int x, y, previousNutrientPoorPlantX, previousNutrientPoorPlantY, previousInfestedPlantX, previousInfestedPlantY;
        boolean addPlant = false;
        // TODO: Do , BRF B:=brf(B, rho);
        if (inputPercept != null) {
            switch ((String) inputPercept.getKey()) {
                case "TankNutrientPoorPlant":
                    parts = ((String)inputPercept.getValue()).split(",");
                    x = Integer.parseInt(parts[0]);
                    y = Integer.parseInt(parts[1]);
                    addPlant = false;

                    if (!returningBeliefs.knowledge.containsKey("TankNutrientPoorPlantX") && !returningBeliefs.knowledge.containsKey("TankNutrientPoorPlantY") ) {
                        addPlant = true;
                    } else {
                        previousNutrientPoorPlantX = (int) returningBeliefs.knowledge.get("TankNutrientPoorPlantX");
                        previousNutrientPoorPlantY = (int) returningBeliefs.knowledge.get("TankNutrientPoorPlantY");
                        if (previousNutrientPoorPlantX != x || previousNutrientPoorPlantY != y)
                            addPlant = true;
                    }

                    if (addPlant) {
                        returningBeliefs.knowledge.put("TankNutrientPoorPlantX", x);
                        returningBeliefs.knowledge.put("TankNutrientPoorPlantY", y);
//                            returningBeliefs.knowledge.put("NutrientPoorPlants", (int) returningBeliefs.knowledge.get("NutrientPoorPlants") + 1);
                        returningBeliefs.knowledge.put("NutrientPoorPlants", 1);
                        returningBeliefs.knowledge.put("TankCanSeeNutrientPoorPlant", true);
                    }
                    break;
                case "BomberInfestedPlant": // TODO: test
                    parts = ((String)inputPercept.getValue()).split(",");
                    x = Integer.parseInt(parts[0]);
                    y = Integer.parseInt(parts[1]);
                    addPlant = false;

                    if (!returningBeliefs.knowledge.containsKey("BomberInfestedPlantX") && !returningBeliefs.knowledge.containsKey("BomberInfestedPlantY") ) {
                        addPlant = true;
                    } else {
                        previousInfestedPlantX = (int) returningBeliefs.knowledge.get("BomberInfestedPlantX");
                        previousInfestedPlantY = (int) returningBeliefs.knowledge.get("BomberInfestedPlantY");
                        if (previousInfestedPlantX != x || previousInfestedPlantY != y)
                            addPlant = true;
                    }

                    if (addPlant) {
                        returningBeliefs.knowledge.put("BomberInfestedPlantX", x);
                        returningBeliefs.knowledge.put("BomberInfestedPlantY", y);
//                        returningBeliefs.knowledge.put("InfestedPlants", (int) returningBeliefs.knowledge.get("InfestedPlants") + 1);
                        returningBeliefs.knowledge.put("InfestedPlants", 1);
                        returningBeliefs.knowledge.put("BomberCanSeeInfestedPlant", true);
                    }
                    break;
            }
        }

        return returningBeliefs;
    }
}
