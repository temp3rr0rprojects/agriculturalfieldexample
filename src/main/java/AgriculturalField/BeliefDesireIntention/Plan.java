package AgriculturalField.BeliefDesireIntention;

import AgriculturalField.HierarchicalTaskNetwork.Planner.HTNGenerator;
import AgriculturalField.HierarchicalTaskNetwork.Planner.HTNPlanner;
import AgriculturalField.HierarchicalTaskNetwork.Planner.WorldState;

import java.util.ArrayList;
import java.util.List;

public class Plan {

    public List<String> actionList = new ArrayList<>();

    public static HTNPlanner planScoutUav(WorldState inputBeliefs, List<String> inputIntentionList) {
        // pi := plan(B, I)

        HTNGenerator htnGenerator = new HTNGenerator();
        HTNPlanner planner = new HTNPlanner(htnGenerator.getScoutUavDomain(inputIntentionList), inputBeliefs);
        planner.generatePlan("Root");

        return planner;
    }

    public static HTNPlanner planBomberUav(WorldState inputBeliefs, List<String> inputIntentionList) {
        // pi := plan(B, I)

        HTNGenerator htnGenerator = new HTNGenerator();
        HTNPlanner planner = new HTNPlanner(htnGenerator.getBomberUavDomain(inputIntentionList), inputBeliefs);
        planner.generatePlan("Root");

        return planner;
    }

    public static HTNPlanner planTankAgv(WorldState inputBeliefs, List<String> inputIntentionList) {
        // pi := plan(B, I)

        HTNGenerator htnGenerator = new HTNGenerator();
        HTNPlanner planner = new HTNPlanner(htnGenerator.getTankAgvDomain(inputIntentionList), inputBeliefs);
        planner.generatePlan("Root");

        return planner;
    }

    public boolean isEmpty() {
        if (actionList.isEmpty())
            return true;
        else
            return false;
    }

    public static String hd(List<String> inputActionList) {
        String returningAction = inputActionList.get(0);

        // TODO: Do stuff hd(pi);

        return returningAction;
    }

    public static List<String> tail(List<String> inputActionList) {
        // TODO: Do stuff, tail(pi);
        return inputActionList.subList(1, inputActionList.size());
    }

    public static boolean sound(List<String> inputActionList, List<String> inputIntensionList, WorldState inputBeliefs) {
        boolean isSound = true;

        // TODO: Do stuff impossible(I, B);

        return isSound;
    }
}