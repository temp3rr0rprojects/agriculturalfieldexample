/*
 * Copyright (C) 2011-2018 Rinde R.S. van Lon
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package AgriculturalField.Entities;

import AgriculturalField.BeliefDesireIntention.*;
import AgriculturalField.HierarchicalTaskNetwork.Planner.HTNPlanner;
import AgriculturalField.HierarchicalTaskNetwork.Planner.WorldState;
import com.github.rinde.rinsim.core.model.comm.*;
import com.github.rinde.rinsim.core.model.rand.RandomProvider;
import com.github.rinde.rinsim.core.model.rand.RandomUser;
import com.github.rinde.rinsim.core.model.road.CollisionPlaneRoadModel;
import com.github.rinde.rinsim.core.model.road.MoveProgress;
import com.github.rinde.rinsim.core.model.road.MovingRoadUser;
import com.github.rinde.rinsim.core.model.road.RoadModel;
import com.github.rinde.rinsim.core.model.time.TickListener;
import com.github.rinde.rinsim.core.model.time.TimeLapse;
import com.github.rinde.rinsim.geom.Point;
import com.google.common.base.Optional;
import javafx.util.Pair;
import org.apache.commons.math3.random.RandomGenerator;

import java.util.*;

public final class AgvAgent
        implements MovingRoadUser, TickListener, RandomUser, CommUser {

  static final int MAX_STUCK_TICK_COUNT = 10;

  static final double TANK_AGV_COMM_RANGE = 300; // Consider Lora Communication range 100 - 500 metres on ground.
  static final double TANK_AGV_COMM_RELIABILITY = 0.9; // High reliability due to slow movement speed
  static final double TANK_AGV_SCAN_RADIUS = 10; // 10 meters

  private Optional<Point> destination = Optional.absent();
  private Optional<RandomGenerator> rng;
  private Optional<CollisionPlaneRoadModel> roadModel;
  private final Point initialPosition;
  private final String name;
  private final double speed;
  private int stuckTickCount;
  private Queue<Point> path;
  // Comm
  private final double range;
  private final double reliability;
  Optional<CommDevice> device;
  private final Set<CommUser> knownAgents;
  long lastReceiveTime;
  static final long LONELINESS_THRESHOLD = 10 * 1000;
  private WorldFacts worldFacts;
  // BDI
  WorldState lastBeliefs;
  List<String> lastDesires;
  List<String> lastIntentions;
  Percept percepts;
  private int scanRadius;
  private List<NutrientPoorPlant> scannedNutrientPoorPlants;
  private TimeLapse timeLapse;
  String alpha;
  Plan lastPi;


  public AgvAgent(Point initPos, String nm, double uavSpeed, WorldFacts inWorldFacts, WorldState inWorldState, List<String> inDesires) {
    initialPosition = initPos;
    name = nm;
    speed = uavSpeed;
    roadModel = Optional.absent();
    destination = Optional.absent();
    rng = Optional.absent();
    path = new LinkedList<>();
    // BDI
    // Desires = all compound tasks, Intentions = filtered compound tasks
    lastBeliefs = inWorldState;
    lastDesires = inDesires;
    lastIntentions = inDesires;
    percepts = new Percept();
    scanRadius = (int)TANK_AGV_SCAN_RADIUS;
    alpha = "";
    lastPi = new Plan();
    // Communication parameters
    device = Optional.absent();
    knownAgents = new HashSet<>();
    range = TANK_AGV_COMM_RANGE;
    reliability = TANK_AGV_COMM_RELIABILITY;
    // World Map facts
    worldFacts = inWorldFacts;
  }

    @Override
    public Optional<Point> getPosition() {
        if (roadModel.get().containsObject(this)) {
            return Optional.of(roadModel.get().getPosition(this));
        }
        return Optional.absent();
    }

    public void scanTick(TimeLapse timeLapse) {

        // Scan for Fertilizer targets
        scannedNutrientPoorPlants = worldFacts.scanNutrientPoorPlantsInRadius(roadModel.get().getPosition(this), scanRadius);

        // Add fertilizer target
        for (NutrientPoorPlant nutrientPoorPlant : scannedNutrientPoorPlants) {
            if (!nutrientPoorPlant.isFertilized) {
                Pair<String, String> pair = new Pair("TankNutrientPoorPlant", String.format("%d,%d", (int) nutrientPoorPlant.position.x, (int) nutrientPoorPlant.position.y));
                if (!percepts.queue.contains(pair))
                    percepts.queue.add(pair);
            }
        }
    }

    @Override
    public void setCommDevice(CommDeviceBuilder builder) {
        if (range >= 0) {
            builder.setMaxRange(range);
        }
        device = Optional.of(builder
                .setReliability(reliability)
                .build());
    }

  @Override
  public void initRoadUser(RoadModel model) {
    roadModel = Optional.of((CollisionPlaneRoadModel) model);
    roadModel.get().addObjectAt(this, initialPosition);
  }

  @Override
  public double getSpeed() {
    return speed;
  }

  public Optional<Point> getDestination() {
    return destination;
  }

  public String getName() {
    return name;
  }

  @Override
  public void tick(TimeLapse timeLapse) {

      this.timeLapse = timeLapse;

      scanTick(timeLapse); // Scan for targets

      bdiTick(timeLapse); // Main BDI loop

      commTick(timeLapse); // Communicate with others
  }

    public Pair getNextPercept() {
        return percepts.queue.poll();
    }

    public boolean succeeded(List<String> inputIntensionList, WorldState inputBeliefs) {
        boolean isSucceeded = false;

        // TODO: Do stuff succeeded(I, B);
        if (inputIntensionList.size() > 0 ) {
            String lastIntention = inputIntensionList.get(0);

            switch (lastIntention) {
                case "NavigateToFertilizer":
                    // TODO: if current loc != station loc -> fail
                    Point fertilizerDestination = new Point((int)inputBeliefs.knowledge.get("TankNutrientPoorPlantX"),
                        (int)inputBeliefs.knowledge.get("TankNutrientPoorPlantY"));
                    if (!roadModel.get().getPosition(this).equals(fertilizerDestination)) {
                        lastBeliefs.knowledge.put("TankFertilizerContainerLevel", 4);
                        isSucceeded = true;
                    }
                    break;
            }
        }

        return isSucceeded;
    }

    public static boolean impossible(List<String> inputIntensionList, WorldState inputBeliefs) {
        boolean isImpossible = false;

        // TODO: Do stuff impossible(I, B);

        return isImpossible;
    }

    public String execute(String alpha, long time) {

      String returningAlpha = alpha;

      switch (alpha){
          case "FindPatrolPoint":
              nextDestination();
              returningAlpha = "";
              break;
        // TODO: execute(pi);
          case "NavigateToNutrientPoorPlant":
              if (lastBeliefs.knowledge.containsKey("TankNutrientPoorPlantX") && lastBeliefs.knowledge.containsKey("TankNutrientPoorPlantY") ) {
                  try {
                      int x = (int)lastBeliefs.knowledge.get("TankNutrientPoorPlantX");
                      int y = (int)lastBeliefs.knowledge.get("TankNutrientPoorPlantY");

                      Point nutrientPoorPlantPosition = new Point(x, y);
                      destination = Optional.of(nutrientPoorPlantPosition);
                      path = new LinkedList<>(roadModel.get().getShortestPathTo(this, destination.get()));

                      final MoveProgress mp = roadModel.get().followPath(this, path, timeLapse);
                      if (roadModel.get().getPosition(this).equals(nutrientPoorPlantPosition) || stuckTickCount >= MAX_STUCK_TICK_COUNT) {
                          returningAlpha = "";
                          stuckTickCount = 0;
                      } else if (mp.distance().getValue().doubleValue() == 0d) {
                          stuckTickCount++;
                      } else {
                          stuckTickCount = 0;
                      }
                  } catch(Exception ex){
                      final MoveProgress mp = roadModel.get().moveTo(this, destination.get(), timeLapse);
                  }
              }
              break;
          case "NavigateToFertilizer":
              try {
                  final MoveProgress mp = roadModel.get().followPath(this, path, timeLapse);
                  if (roadModel.get().getPosition(this).equals(worldFacts.fertilizerStation.position) || stuckTickCount >= MAX_STUCK_TICK_COUNT) {
                      returningAlpha = "";
                      stuckTickCount = 0;
                  } else if (mp.distance().getValue().doubleValue() == 0d) {
                      stuckTickCount++;
                  } else {
                      stuckTickCount = 0;
                  }
              } catch (Exception ex) {
                  final MoveProgress mp = roadModel.get().moveTo(this, destination.get(), timeLapse);
              }
              break;
          case "PickUpBattery":
              returningAlpha = "";
              worldFacts.addLog(time + "," + alpha + "," + this.name);
              break;
          case "PickUpFertilizer":
              returningAlpha = "";
              break;
          case "NavigateToBattery":
              try {
                  final MoveProgress mp = roadModel.get().followPath(this, path, timeLapse);
                  if (roadModel.get().getPosition(this).equals(worldFacts.batteryStation.position) || stuckTickCount >= MAX_STUCK_TICK_COUNT) {
                      returningAlpha = "";
                      stuckTickCount = 0;
                  } else if (mp.distance().getValue().doubleValue() == 0d) {
                      stuckTickCount++;
                  } else {
                      stuckTickCount = 0;
                  }
              } catch (Exception ex) {
                  final MoveProgress mp = roadModel.get().moveTo(this, destination.get(), timeLapse);
              }
              break;
          case "NavigateToPatrolPoint":
              try {
                  final MoveProgress mp = roadModel.get().followPath(this, path, timeLapse);
                  if (roadModel.get().getPosition(this).equals(destination.get()) || stuckTickCount >= MAX_STUCK_TICK_COUNT) {
                      returningAlpha = "";
                      stuckTickCount = 0;
                  } else if (mp.distance().getValue().doubleValue() == 0d) {
                      stuckTickCount++;
                  } else {
                      stuckTickCount = 0;
                  }
              } catch (Exception ex) {
                  final MoveProgress mp = roadModel.get().moveTo(this, destination.get(), timeLapse);
              }
            break;
          case "FindFertilizer":
              // It is a known point, a WorldFact the coordinates of the FertilizerStation
              destination = Optional.of(worldFacts.fertilizerStation.position);
              path = new LinkedList<>(roadModel.get().getShortestPathTo(this, destination.get()));
              returningAlpha = "";
              break;
          case "FindBattery":
              // It is a known point, a WorldFact the coordinates of the batteryStation
              destination = Optional.of(worldFacts.batteryStation.position);
              path = new LinkedList<>(roadModel.get().getShortestPathTo(this, destination.get()));
              returningAlpha = "";
              break;
          case "UseFertilizer":
              // TODO: remove plant from knowledge
              if (lastBeliefs.knowledge.containsKey("TankNutrientPoorPlantX") && lastBeliefs.knowledge.containsKey("TankNutrientPoorPlantY") ) {

                  int x = (int)lastBeliefs.knowledge.get("TankNutrientPoorPlantX");
                  int y = (int)lastBeliefs.knowledge.get("TankNutrientPoorPlantY");

                  Point nutrientPoorPlantPosition = new Point(x, y);
                   if (!worldFacts.fertilizePlant(nutrientPoorPlantPosition)) {
                       lastBeliefs.knowledge.remove("TankNutrientPoorPlantX");
                       lastBeliefs.knowledge.remove("TankNutrientPoorPlantY");
                       lastBeliefs.knowledge.put("TankNutrientPoorPlant", null);
                       lastBeliefs.knowledge.put("TankCanSeeNutrientPoorPlant", false);

                   } else
                       worldFacts.addLog(time + "," + alpha + "," + this.name);
              }
              returningAlpha = "";
              break;
          default:
              returningAlpha = returningAlpha; // For debugging, finding non-trivial actions
              break;
      }

      return returningAlpha;
    }


    public static boolean reconsider(List<String> inputIntensionList, WorldState inputBeliefs) {
        boolean doReconsider = false;

        // TODO: Do stuff impossible(I, B);

        return doReconsider;
    }

  public void bdiTick(TimeLapse timeLapse) {

      WorldState beliefs = lastBeliefs; // Beliefs <-> HTN WorldState
      List<String> intentions = lastIntentions;

      // Naive BDI version 1
      Pair rho = getNextPercept(); // Sensor stimuli
      if (rho != null)
          beliefs = Belief.beliefRevisionFunction(beliefs, rho); // B:= brf(B, percepts);

      // Deliberation
      List<String> desires = Desire.options(beliefs, intentions); // D:= options(B, I); Desires -> HTN (Top) level Compound tasks
      intentions = Intention.filter(beliefs, desires, intentions); // I:= filter(B, D, I); Intentions -> Filtered HTN (Top) level Compound tasks
      if (beliefs != lastBeliefs || intentions != lastIntentions || alpha == "") {

          //Plan pi = new Plan();
          Plan pi = lastPi;
          pi.actionList = lastPi.actionList;
          if (pi.actionList.size() == 0) {
              HTNPlanner htnPlanner = Plan.planTankAgv(beliefs, intentions);
              pi.actionList = (List<String>)htnPlanner.finalPlan;
              beliefs = htnPlanner.finalWorldState;
          }
          alpha = Plan.hd(pi.actionList); // alpha:= hd(pi);
          pi.actionList = Plan.tail(pi.actionList); // pi:= tail(pi);
          alpha = execute(alpha, timeLapse.getTime());

         lastPi.actionList = pi.actionList;
      }

      if (alpha != "")
          alpha = execute(alpha, timeLapse.getTime());

      lastBeliefs = beliefs;
      lastDesires = desires;
      lastIntentions = intentions;
  }

    public void commTick(TimeLapse timeLapse) {

        if (device.get().getUnreadCount() > 0) { // check if we have messages
            lastReceiveTime = timeLapse.getStartTime();
            final List<Message> messages = device.get().getUnreadMessages();

            for (final Message message : messages) { // reading all unread messages

                if (message.getContents() == Messages.WHO_ARE_YOU) {
                    device.get().send(new MyNameIs(name), message.getSender()); // if someone asks us who we are, we give an polite answer
                } else if (message.getContents() instanceof MyNameIs) { // if someone tells us their name, we remember who they are.
                    final String nameOfOther = ((MyNameIs) message.getContents()).getName(); // cast the message to read the name
                    System.out.println("I, " + name + ", just met " + nameOfOther);
                    knownAgents.add(message.getSender());
                }

                // Multi-Agent Communication: Scout returns Target Plant coordinates
                if (message.getContents() == Messages.NUTRIENT_POOR_PLANT_COORDINATES) {
                    device.get().send(Messages.GET_NUTRIENT_POOR_PLANT_COORDINATES, message.getSender());
                    System.out.println("I, " + name + ", just learned from " + message.getSender()  + ", there is nutrient poor plant");
                }

                // MAS HTN Planning: adds Compound Task as a percept, for the next HTN planning
                if (message.getContents() instanceof  PlantCoordinatesAre) {
                    final String nutrientPoorPlantCoordinates = ((PlantCoordinatesAre) message.getContents()).getCoordinates();
                    Pair<String, String> pair = new Pair("TankNutrientPoorPlant", nutrientPoorPlantCoordinates);

                    if (!percepts.queue.contains(pair)) {
                        percepts.queue.add(pair);
                        System.out.println("I, " + name + ", just learned from " + message.getSender()  + ", there is nutrient poor plant on " + nutrientPoorPlantCoordinates);
                        worldFacts.addLog(timeLapse.getTime() + "," + "TankNutrientPoorPlant" + "," + this.name);
                    }
                }

                if (!knownAgents.contains(message.getSender())) // if we don't know the sender, we ask who they are
                    device.get().send(Messages.WHO_ARE_YOU, message.getSender());
            }
            device.get().broadcast(Messages.NICE_TO_MEET_YOU); // when we have non-zero unread messages, we always broadcast "nice to meet you" to everyone within range.
        } else if (device.get().getReceivedCount() == 0) { // our first message
            device.get().broadcast(Messages.HELLO_WORLD);
        } else if (timeLapse.getStartTime() - lastReceiveTime > LONELINESS_THRESHOLD) { // when we haven't received anything for a while, we become anxious :(
            device.get().broadcast(Messages.WHERE_IS_EVERYBODY);
        }
    }

    enum Messages implements MessageContents {
        HELLO_WORLD, NICE_TO_MEET_YOU, WHERE_IS_EVERYBODY, WHO_ARE_YOU, NUTRIENT_POOR_PLANT_COORDINATES, INFESTED_PLANT_COORDINATES, GET_INFESTED_PLANT_COORDINATES, GET_NUTRIENT_POOR_PLANT_COORDINATES
    }

    static class PlantCoordinatesAre implements MessageContents {
        private final String coordinates;

        PlantCoordinatesAre(String coord) { // TODO: point class?
            coordinates = coord;
        }

        String getCoordinates() {
            return coordinates;
        }
    }

    static class MyNameIs implements MessageContents {
        private final String name;

        MyNameIs(String nm) {
            name = nm;
        }

        String getName() {
            return name;
        }
    }

  private void nextDestination() {
    destination = Optional.of(roadModel.get().getRandomPosition(rng.get())); // TODO: Could connect route patterns here
    path = new LinkedList<>(roadModel.get().getShortestPathTo(this, destination.get()));
  }

  @Override
  public void afterTick(TimeLapse timeLapse) {}

  @Override
  public String toString() {
    return String.format("[%s %s]", getClass().getSimpleName(), name);
  }

  @Override
  public void setRandomGenerator(RandomProvider provider) {
    rng = Optional.of(provider.newInstance());
  }

}