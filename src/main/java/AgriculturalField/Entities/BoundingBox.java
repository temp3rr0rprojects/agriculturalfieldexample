package AgriculturalField.Entities;

import com.github.rinde.rinsim.geom.Point;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

public class BoundingBox {
  public Point TopLeft;
  public Point BottomRight;

  BoundingBox(Point topLeft, Point bottomRight) {
    TopLeft = topLeft;
    BottomRight = bottomRight;
  }

    public LinkedList<BoundingBox> getDivided(boolean divideHorizontal) {
        if (divideHorizontal) {
            return new LinkedList<>(Arrays.asList(new BoundingBox(TopLeft, new Point(BottomRight.x, TopLeft.y / 2)), new BoundingBox(new Point(TopLeft.x, TopLeft.y / 2), BottomRight)));
        } else {
            return new LinkedList<>(Arrays.asList(new BoundingBox(TopLeft, new Point(BottomRight.x / 2, BottomRight.y)), new BoundingBox(new Point(BottomRight.x / 2, TopLeft.y), BottomRight)));
        }
    }

    public ArrayList<BoundingBox> getDivided3(boolean divideHorizontal) {
        if (divideHorizontal) {
            return new ArrayList<>(Arrays.asList(new BoundingBox(TopLeft, new Point(BottomRight.x, TopLeft.y / 2)), new BoundingBox(new Point(TopLeft.x, TopLeft.y / 2), BottomRight)));
        } else {
            return new ArrayList<>(Arrays.asList(new BoundingBox(TopLeft, new Point(BottomRight.x / 2, BottomRight.y)), new BoundingBox(new Point(BottomRight.x / 2, TopLeft.y), BottomRight)));
        }
    }


    public Pair<BoundingBox, BoundingBox> getDivided2(boolean divideHorizontal) {
        if (divideHorizontal) {
            return new Pair<>(new BoundingBox(TopLeft, new Point(BottomRight.x, TopLeft.y / 2)),
                    new BoundingBox(new Point(TopLeft.x, TopLeft.y / 2), BottomRight));
        } else {
            return new Pair<>(new BoundingBox(TopLeft, new Point(BottomRight.x / 2, BottomRight.y)),
                    new BoundingBox(new Point(BottomRight.x / 2, TopLeft.y), BottomRight));
        }
    }

    public LinkedList<BoundingBox> getPopulatedBboxList(BoundingBox initBoundingBox, int elements) {
      LinkedList<BoundingBox> returningLinkedList = new LinkedList<>();
      returningLinkedList.push(initBoundingBox);
        LinkedList<BoundingBox> newLinkedList = new LinkedList<>();

      int counter = 0;
      boolean vertical = true;
        for (BoundingBox bx: returningLinkedList) {
            newLinkedList.addAll(bx.getDivided(vertical));
            if (vertical)
                vertical = false;
            else
                vertical = true;
            counter++;
            if (counter > elements)
                break;
        }

      return returningLinkedList;
    }
}
