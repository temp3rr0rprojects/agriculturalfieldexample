package AgriculturalField.Entities;

import com.github.rinde.rinsim.geom.Point;

public class InfestedPlant extends PlantTarget {

    public boolean isInfested = true;

    public InfestedPlant(Point position) { super(position); }

    public void sprayPesticide() { isInfested = false; }
}
