package AgriculturalField.Entities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class Logger {
    public final List<String> logs;
    public int numBombers = 0;
    public int numTanks = 0;
    public int numScouts = 0;
    public String fileName;


    public Logger(String fileName, int numScouts, int numBombers, int numTanks) {
        logs = new ArrayList<>();
        this.numScouts = numScouts;
        this.numBombers = numBombers;
        this.numTanks = numTanks;
        this.fileName = "scouts" + numScouts + "_bombers" + numBombers
                + "_tanks" + numTanks + "_" + fileName + ".csv";
        logs.add("time,event,agent,scouts,bombers,tanks\n");
        logToCsv();
    }

    public void addLog(String logRow) {
        logs.add(logRow +"," + numScouts + "," + numBombers + "," + numTanks + "\n");
        logToCsv();
    }

    public void logToCsv() {
        PrintWriter pw = null;
        try {
            pw = new PrintWriter(new PrintWriter(new FileOutputStream(new File(fileName), true)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        StringBuilder builder = new StringBuilder();
        for(String logRow : logs)
            builder.append(logRow);
        pw.write(builder.toString());
        pw.close();
        logs.clear();
    }
}
