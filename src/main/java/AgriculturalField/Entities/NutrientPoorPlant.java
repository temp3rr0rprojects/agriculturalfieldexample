package AgriculturalField.Entities;

import com.github.rinde.rinsim.geom.Point;

public class NutrientPoorPlant extends PlantTarget {

    public boolean isFertilized = false;

    public NutrientPoorPlant(Point position) { super(position); }

    public void fertilize() { isFertilized = true; }
}
