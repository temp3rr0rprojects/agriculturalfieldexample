package AgriculturalField.Entities;

import com.github.rinde.rinsim.geom.Point;

public class PlantTarget {
    public Point position;

    public static double radius = 1; // 1 meters

    public PlantTarget(Point position) { this.position = position; }

    public Point getPosition() {
        return position;
    }
}
