package AgriculturalField.Entities;

import com.github.rinde.rinsim.geom.Point;

public class Station {
    public Point position;
    public boolean isOccupied = false;
    public static double radius = 2; // 2 meters

    public Station(Point position) { this.position = position; }

    public Point getPosition() {
        return position;
    }
}
