/*
 * Copyright (C) 2011-2018 Rinde R.S. van Lon
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package AgriculturalField.Entities;

import AgriculturalField.BeliefDesireIntention.*;
import AgriculturalField.HierarchicalTaskNetwork.Planner.HTNPlanner;
import AgriculturalField.HierarchicalTaskNetwork.Planner.WorldState;
import com.github.rinde.rinsim.core.model.comm.CommDevice;
import com.github.rinde.rinsim.core.model.comm.CommDeviceBuilder;
import com.github.rinde.rinsim.core.model.comm.CommUser;
import com.github.rinde.rinsim.core.model.comm.Message;
import com.github.rinde.rinsim.core.model.rand.RandomProvider;
import com.github.rinde.rinsim.core.model.rand.RandomUser;
import com.github.rinde.rinsim.core.model.road.CollisionPlaneRoadModel;
import com.github.rinde.rinsim.core.model.road.MoveProgress;
import com.github.rinde.rinsim.core.model.road.MovingRoadUser;
import com.github.rinde.rinsim.core.model.road.RoadModel;
import com.github.rinde.rinsim.core.model.time.TickListener;
import com.github.rinde.rinsim.core.model.time.TimeLapse;
import com.github.rinde.rinsim.geom.Point;
import com.google.common.base.Optional;
import javafx.util.Pair;
import org.apache.commons.math3.random.RandomGenerator;

import java.util.*;

public final class UavAgent
    implements MovingRoadUser, TickListener, RandomUser, CommUser {

  static final int MAX_STUCK_TICK_COUNT = 10;
  // Consider Lora Communication range 500 - 1000 metres air-to-air and air-to-ground.
  static final double SCOUT_UAV_COMM_RANGE = 1000; // 1000 meters for Scout UAV Fixed Wing, they fly very high for aerial footage
  static final double BOMBER_UAV_COMM_RANGE = 600; // 500 meters for Bomber UAV Multi-rotors, they fly low (to spray insecticide)
  static final double SCOUT_UAV_COMM_RELIABILITY = 0.6; // Scout UAV: lower due to higher speed & altitude vs Bomber UAV
  static final double BOMBER_UAV_COMM_RELIABILITY = 0.7;
  static final double SCOUT_UAV_SCAN_RADIUS = 250; // Fertilizer targets: 1.5km x 1.5 km multi-spectral image radius (120m altitude)
  static final double BOMBER_UAV_SCAN_RADIUS = 100; // Pesticide targets: 500m x 500m leaf dysmorphia (40m altitude)

  private final double range; // Lora range
  private final double reliability; // Lora communication reliability
  Optional<CommDevice> device;
  private final Set<CommUser> knownAgents;
  long lastReceiveTime; // When was the last comm msg received
  static final long LONELINESS_THRESHOLD = 10 * 1000; // Incoming msg time threshold. TODO: test threshold in practice comm user stuff

  private Optional<Point> destination = Optional.absent();
  private Optional<RandomGenerator> rng;
  private Optional<CollisionPlaneRoadModel> roadModel;
  private final Point initialPosition;
  private final String name;
  private final double speed;
  private final UavType type;
  private int stuckTickCount;
  private Queue<Point> path;
  private LinkedList<Point> wayPoints = new LinkedList<>();
  private int scanRadius;
  private int planeSize;
  private double vehicleLength = 1.5;
  public WorldFacts worldFacts;
  // BDI
  private List<InfestedPlant> scannedInfestedPlants;
  private List<NutrientPoorPlant> scannedNutrientPoorPlants;
    private TimeLapse timeLapse;
    WorldState lastBeliefs;
    List<String> lastDesires;
    List<String> lastIntentions;
    String alpha;
    Plan lastPi;
    Percept percepts;

  public UavAgent(Point initPos, String nm, double uavSpeed, UavType uavType, WorldFacts inWorldFacts, WorldState inWorldState, List<String> inDesires) {
    initialPosition = initPos;
    name = nm;
    speed = uavSpeed;
    roadModel = Optional.absent();
    destination = Optional.absent();
    rng = Optional.absent();
    type = uavType;
    planeSize = (int)inWorldFacts.planeSize;
    // BDI parameters
    // Desires = all compound tasks, Intentions = filtered compound tasks
    lastBeliefs = inWorldState;
    lastDesires = inDesires;
    lastIntentions = inDesires;
    percepts = new Percept();
    alpha = "";
    lastPi = new Plan();
    // Communication parameters
    device = Optional.absent();
    knownAgents = new HashSet<>();
    if (uavType == UavType.Scout) {
        range = SCOUT_UAV_COMM_RANGE;
        reliability = SCOUT_UAV_COMM_RELIABILITY;
        scanRadius = (int)SCOUT_UAV_SCAN_RADIUS;
    } else {
        range = BOMBER_UAV_COMM_RANGE;
        reliability = BOMBER_UAV_COMM_RELIABILITY;
        scanRadius = (int)BOMBER_UAV_SCAN_RADIUS;
    }
    // World Map facts
    worldFacts = inWorldFacts;
  }

  @Override
  public Optional<Point> getPosition() {
    if (roadModel.get().containsObject(this)) {
      return Optional.of(roadModel.get().getPosition(this));
    }
    return Optional.absent();
  }

  @Override
  public void setCommDevice(CommDeviceBuilder builder) {
    if (range >= 0) {
      builder.setMaxRange(range);
    }
    device = Optional.of(builder.setReliability(reliability).build());
  }

  @Override
  public void initRoadUser(RoadModel model) {
    roadModel = Optional.of((CollisionPlaneRoadModel) model);
    roadModel.get().addObjectAt(this, initialPosition);
  }

    public String execute(String alpha, long time) {

        String returningAlpha = alpha;

        switch (alpha){
            case "FindPatrolPoint":
                nextDestination();
                returningAlpha = "";
                break;
            // TODO: execute(pi);
            case "NavigateToInfestedPlant":
                if (lastBeliefs.knowledge.containsKey("BomberInfestedPlantX") && lastBeliefs.knowledge.containsKey("BomberInfestedPlantY") ) {
                    try {
                        int x = (int)lastBeliefs.knowledge.get("BomberInfestedPlantX");
                        int y = (int)lastBeliefs.knowledge.get("BomberInfestedPlantY");

                        Point infestedPlantPosition = new Point(x, y);
                        destination = Optional.of(infestedPlantPosition);
                        path = new LinkedList<>(roadModel.get().getShortestPathTo(this, destination.get()));

                        final MoveProgress mp = roadModel.get().followPath(this, path, timeLapse);
                        if (roadModel.get().getPosition(this).equals(infestedPlantPosition) || stuckTickCount >= MAX_STUCK_TICK_COUNT) {
                            returningAlpha = "";
                            stuckTickCount = 0;
                        } else if (mp.distance().getValue().doubleValue() == 0d) {
                            stuckTickCount++;
                        } else {
                            stuckTickCount = 0;
                        }
                    } catch(Exception ex){
                        final MoveProgress mp = roadModel.get().moveTo(this, destination.get(), timeLapse);
                    }
                }
                break;
            case "NavigateToPesticide":
                try {
                    final MoveProgress mp = roadModel.get().followPath(this, path, timeLapse);
                    if (roadModel.get().getPosition(this).equals(worldFacts.pesticideStation.position) || stuckTickCount >= MAX_STUCK_TICK_COUNT) {
                        returningAlpha = "";
                        stuckTickCount = 0;
                    } else if (mp.distance().getValue().doubleValue() == 0d) {
                        stuckTickCount++;
                    } else {
                        stuckTickCount = 0;
                    }
                } catch (Exception ex) {
                    final MoveProgress mp = roadModel.get().moveTo(this, destination.get(), timeLapse);
                }
                break;
            case "PickUpBattery":
                worldFacts.addLog(time + "," + alpha + "," + this.name);
                returningAlpha = "";
                break;
            case "PickUpPesticide":
                returningAlpha = "";
                break;
            case "NavigateToBattery":
                try {
                    final MoveProgress mp = roadModel.get().followPath(this, path, timeLapse);
                    if (roadModel.get().getPosition(this).equals(worldFacts.batteryStation.position) || stuckTickCount >= MAX_STUCK_TICK_COUNT) {
                        returningAlpha = "";
                        stuckTickCount = 0;
                    } else if (mp.distance().getValue().doubleValue() == 0d) {
                        stuckTickCount++;
                    } else {
                        stuckTickCount = 0;
                    }
                } catch (Exception ex) {
                    final MoveProgress mp = roadModel.get().moveTo(this, destination.get(), timeLapse);
                }
                break;
            case "NavigateToPatrolPoint":
                try {
                    final MoveProgress mp = roadModel.get().followPath(this, path, timeLapse);
                    if (roadModel.get().getPosition(this).equals(destination.get()) || stuckTickCount >= MAX_STUCK_TICK_COUNT) {
                        returningAlpha = "";
                        stuckTickCount = 0;
                    } else if (mp.distance().getValue().doubleValue() == 0d) {
                        stuckTickCount++;
                    } else {
                        stuckTickCount = 0;
                    }
                } catch (Exception ex) {
                    final MoveProgress mp = roadModel.get().moveTo(this, destination.get(), timeLapse);
                }
                break;
            case "FindPesticide":
                // It is a known point, a WorldFact the coordinates of the FertilizerStation
                destination = Optional.of(worldFacts.pesticideStation.position);
                path = new LinkedList<>(roadModel.get().getShortestPathTo(this, destination.get()));
                returningAlpha = "";
                break;
            case "FindBattery":
                // It is a known point, a WorldFact the coordinates of the batteryStation
                destination = Optional.of(worldFacts.batteryStation.position);
                path = new LinkedList<>(roadModel.get().getShortestPathTo(this, destination.get()));
                returningAlpha = "";
                break;
            case "UsePesticide":
                if (lastBeliefs.knowledge.containsKey("BomberInfestedPlantX") && lastBeliefs.knowledge.containsKey("BomberInfestedPlantY") ) {

                    int x = (int)lastBeliefs.knowledge.get("BomberInfestedPlantX");
                    int y = (int)lastBeliefs.knowledge.get("BomberInfestedPlantY");

                    Point nutrientPoorPlantPosition = new Point(x, y);
                    if (!worldFacts.sprayPlant(nutrientPoorPlantPosition)) {
                        lastBeliefs.knowledge.remove("BomberInfestedPlantX");
                        lastBeliefs.knowledge.remove("BomberInfestedPlantY");
                        lastBeliefs.knowledge.put("BomberInfestedPlant", null);
                        lastBeliefs.knowledge.put("BomberCanSeeInfestedPlant", false);

                    } else
                        worldFacts.addLog(time + "," + alpha + "," + this.name);
                }

                returningAlpha = "";
                break;
            default:
                returningAlpha = returningAlpha; // For debugging, finding non-trivial actions
                break;
        }

        return returningAlpha;
    }

  @Override
  public double getSpeed() {
    return speed;
  }

  /**
   * Converts speed from kilometers/hour to meters/second.
   * @param kilometresPerHour Double km/h.
   * @return Double m/s.
   */
  public static double getMetersPerSecond(double kilometresPerHour) { return  (kilometresPerHour * 1000) / 3600; }

  public Optional<Point> getDestination() {
    return destination;
  }

  public String getName() {
    return name;
  }

  public UavType getType() {
    return type;
  }

  @Override
  public void tick(TimeLapse timeLapse) {

    this.timeLapse = timeLapse;

    scanTick(timeLapse); // Scan for targets

    bdiTick(timeLapse); // Main BDI loop

    commTick(timeLapse); // Communicate with others
  }

    public void bdiTick(TimeLapse timeLapse) {

        WorldState beliefs = lastBeliefs; // Beliefs <-> HTN WorldState
        List<String> intentions = lastIntentions;

        Pair rho = getNextPercept(); // Sensor stimuli
        if (rho != null)
            beliefs = Belief.beliefRevisionFunction(beliefs, rho); // B:= brf(B, percepts);

        // Deliberation
        List<String> desires = Desire.options(beliefs, intentions); // D:= options(B, I); Desires -> HTN (Top) level Compound tasks
        intentions = Intention.filter(beliefs, desires, intentions); // I:= filter(B, D, I); Intentions -> Filtered HTN (Top) level Compound tasks
        if (beliefs != lastBeliefs || intentions != lastIntentions || alpha == "") {

            Plan pi = lastPi;
            pi.actionList = lastPi.actionList;
            if (pi.actionList.size() == 0) {

                HTNPlanner htnPlanner;
                if (type == UavType.Bomber)
                    htnPlanner = Plan.planBomberUav(beliefs, intentions);
                else
                    htnPlanner = Plan.planScoutUav(beliefs, intentions);

                pi.actionList = (List<String>)htnPlanner.finalPlan;
                beliefs = htnPlanner.finalWorldState;


            }
            alpha = Plan.hd(pi.actionList); // alpha:= hd(pi);
            pi.actionList = Plan.tail(pi.actionList); // pi:= tail(pi);
            alpha = execute(alpha, timeLapse.getTime());

            lastPi.actionList = pi.actionList;
        }

        if (alpha != "")
            alpha = execute(alpha, timeLapse.getTime());

        lastBeliefs = beliefs;
        lastDesires = desires;
        lastIntentions = intentions;
    }

  public Pair getNextPercept() {
      return percepts.queue.poll();
  }

  public void scanTick(TimeLapse timeLapse) {

      // Scan for Pesticide targets
      scannedInfestedPlants = worldFacts.scanInfestedPlantsInRadius(roadModel.get().getPosition(this), scanRadius);

      if (type == UavType.Bomber) { // If bomber UAV, add pesticide target
          for (InfestedPlant scannedInfestedPlant : scannedInfestedPlants) {
              if (scannedInfestedPlant.isInfested) {
                  Pair<String, String> pair = new Pair("BomberInfestedPlant", String.format("%d,%d", (int) scannedInfestedPlant.position.x, (int) scannedInfestedPlant.position.y));
                  if (!percepts.queue.contains(pair))
                      percepts.queue.add(pair);
              }
          }
      }

      // Scan for Fertilizer targets (only Scout)
      if (type == UavType.Scout)
        scannedNutrientPoorPlants = worldFacts.scanNutrientPoorPlantsInRadius(roadModel.get().getPosition(this), scanRadius);
  }

    public void commTick(TimeLapse timeLapse) {

        if (device.get().getUnreadCount() > 0) { // check if we have messages
            lastReceiveTime = timeLapse.getStartTime();
            final List<Message> messages = device.get().getUnreadMessages();

            for (final Message message : messages) { // reading all unread messages

                if (message.getContents() == AgvAgent.Messages.WHO_ARE_YOU) {
                    device.get().send(new AgvAgent.MyNameIs(name), message.getSender()); // if someone asks us who we are, we give an polite answer
                } else if (message.getContents() instanceof AgvAgent.MyNameIs) { // if someone tells us their name, we remember who they are.
                    final String nameOfOther = ((AgvAgent.MyNameIs) message.getContents()).getName(); // cast the message to read the name
                    System.out.println("I, " + name + ", just met " + nameOfOther);
                    knownAgents.add(message.getSender());
                }

                // Multi-Agent Communication: Scout returns Target Plant coordinates
                if (type == UavType.Bomber) {
                    if (message.getContents() == AgvAgent.Messages.INFESTED_PLANT_COORDINATES) {
                        device.get().send(AgvAgent.Messages.GET_INFESTED_PLANT_COORDINATES, message.getSender());
                        System.out.println("I, " + name + ", just learned from " + message.getSender()  + ", there is infested plant");
                    }

                    // MAS HTN Planning: adds Compound Task as a percept, for the next HTN planning
                    if (message.getContents() instanceof  AgvAgent.PlantCoordinatesAre) {
                        final String infestedPlantCoordinates = ((AgvAgent.PlantCoordinatesAre) message.getContents()).getCoordinates();
                        Pair<String, String> pair = new Pair("BomberInfestedPlant", infestedPlantCoordinates);
                        if (!percepts.queue.contains(pair)) {
                            percepts.queue.add(pair);
                            System.out.println("I, " + name + ", just learned from " + message.getSender()  + ", there is infested plant on " + infestedPlantCoordinates);
                            worldFacts.addLog(timeLapse.getTime() + "," + "BomberInfestedPlant" + "," + this.name);
                        }
                    }
                } else if (type == UavType.Scout) {

                    // Multi-Agent Communication: Scout sends back found Target Plant coordinates

                    if (message.getContents() == AgvAgent.Messages.GET_INFESTED_PLANT_COORDINATES) {
                        for (InfestedPlant scannedInfestedPlant : scannedInfestedPlants) {
                            String infestedPlantCoordinates = String.format("%d,%d", (int)scannedInfestedPlant.position.x, (int)scannedInfestedPlant.position.y);
                            device.get().send(new AgvAgent.PlantCoordinatesAre(infestedPlantCoordinates), message.getSender());
                            System.out.println("I, " + name + ", sending info on infested plant on " + infestedPlantCoordinates + " to " + message.getSender());
                        }
                    }

                    if (message.getContents() == AgvAgent.Messages.GET_NUTRIENT_POOR_PLANT_COORDINATES) {
                        for (NutrientPoorPlant scannedNutrientPoorPlant : scannedNutrientPoorPlants) {
                            String nutrientPoorPlantCoordinates = String.format("%d,%d", (int)scannedNutrientPoorPlant.position.x, (int)scannedNutrientPoorPlant.position.y);
                            device.get().send(new AgvAgent.PlantCoordinatesAre(nutrientPoorPlantCoordinates), message.getSender());
                            System.out.println("I, " + name + ", sending info on nutrient poor plant on " + nutrientPoorPlantCoordinates + " to " + message.getSender());
                        }
                    }
                }

                if (!knownAgents.contains(message.getSender())) // if we don't know the sender, we ask who they are
                    device.get().send(AgvAgent.Messages.WHO_ARE_YOU, message.getSender());
            }

            device.get().broadcast(AgvAgent.Messages.NICE_TO_MEET_YOU); // when we have non-zero unread messages, we always broadcast "nice to meet you" to everyone within range.
        } else if (device.get().getReceivedCount() == 0) { // our first message
            device.get().broadcast(AgvAgent.Messages.HELLO_WORLD);
        } else if (timeLapse.getStartTime() - lastReceiveTime > LONELINESS_THRESHOLD) { // when we haven't received anything for a while, we become anxious :(
            device.get().broadcast(AgvAgent.Messages.WHERE_IS_EVERYBODY);
        }

        // Multi-Agent Communication: Scout broadcasts that it found Target Plants
        if (type == UavType.Scout) {
            for (InfestedPlant scannedInfestedPlant : scannedInfestedPlants) {
                if (scannedInfestedPlant.isInfested) {
                    String infestedPlantCoordinates = String.format("%d,%d", (int) scannedInfestedPlant.position.x, (int) scannedInfestedPlant.position.y);
                    device.get().broadcast(AgvAgent.Messages.INFESTED_PLANT_COORDINATES);
                    System.out.println("I, " + name + ", just found infested plant on " + infestedPlantCoordinates);
                }
            }
            for (NutrientPoorPlant scannedNutrientPoorPlant : scannedNutrientPoorPlants) {
                if (!scannedNutrientPoorPlant.isFertilized) {
                    String nutrientPoorPlantCoordinates = String.format("%d,%d", (int) scannedNutrientPoorPlant.position.x, (int) scannedNutrientPoorPlant.position.y);
                    device.get().broadcast(AgvAgent.Messages.NUTRIENT_POOR_PLANT_COORDINATES);
                    System.out.println("I, " + name + ", just found nutrient poor plant on " + nutrientPoorPlantCoordinates);
                }
            }
        }
    }

  private void nextDestination() {
    if (this.type == UavType.Scout)
      destination = Optional.of(getNextWayPoint(scanRadius, planeSize, vehicleLength));
    else {
        destination = Optional.of(roadModel.get().getRandomPosition(rng.get())); // TODO: Could connect route patterns here
    }
      path = new LinkedList<>(roadModel.get().getShortestPathTo(this, destination.get()));
  }

  private Point getNextWayPoint(int scanRadius, int planeSize, double vehicleLength) {
    if (wayPoints.size() == 0)
      wayPoints = getSpiralWayPoints(scanRadius, planeSize, vehicleLength); // Re-calculate the spiral plan
    return wayPoints.pop();
  }

  /***
   * Calculates a linked list with spiral way-points for the full plane.
   * @param scanRadius UAV scan radius, used for offset on the plane limits.
   * @param planeWidth Plane width & height.
   * @return Returns a linked list with Points for spiral scanning of a plane.
   */
  private LinkedList<Point> getSpiralWayPoints(int scanRadius, int planeWidth, double vehicleLength){
    LinkedList<Point> returningLinkedList = new LinkedList<>();

    int yMin = scanRadius;
    int yMax = planeWidth - scanRadius;
    int x = scanRadius;
    int y = yMin;
    boolean moveDown = true;

    returningLinkedList.add(new Point(x, y)); // Init point in offset
    do {
      if (moveDown){
        y = yMax;
        moveDown = false;
      } else {
        y = yMin;
        moveDown = true;
      }
      if (x < planeWidth + vehicleLength && y < planeWidth + vehicleLength)
        returningLinkedList.add(new Point(x, y)); // Up or Down
      x += 2 * scanRadius;
      if (x < planeWidth + vehicleLength && y < planeWidth + vehicleLength)
        returningLinkedList.add(new Point(x, y)); // Left
    } while (x < planeWidth + vehicleLength && y < planeWidth + vehicleLength);

    return returningLinkedList;
  }

  @Override
  public void afterTick(TimeLapse timeLapse) {}

  @Override
  public String toString() {
    return String.format("[%s %s]", getClass().getSimpleName(), name);
  }

  @Override
  public void setRandomGenerator(RandomProvider provider) {
    rng = Optional.of(provider.newInstance());
  }
}
