package AgriculturalField.Entities;

public enum UavType {
    Bomber, // Bomber UAV, flies low & slow, sprays pesticide
    Scout // Scout UAV, flies fast & high, identifies pesticide & fertilizer targets
}
