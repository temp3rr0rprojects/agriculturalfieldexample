package AgriculturalField.Entities;

import com.github.rinde.rinsim.geom.Point;

import java.util.ArrayList;
import java.util.List;

public class WorldFacts {

    // TODO: make it singleton

    public final BatteryStation batteryStation;
    public final FertilizerStation fertilizerStation;
    public final PesticideStation pesticideStation;
    public List<NutrientPoorPlant> nutrientPoorPlantList = new ArrayList<NutrientPoorPlant>();
    public List<InfestedPlant> infestedPlantList = new ArrayList<InfestedPlant>();
    public final double planeSize;
    public Logger logger;

    public WorldFacts(double planeSize) {

        this.planeSize = planeSize;

        // Set stations at the top
        batteryStation = new BatteryStation(new Point((int)Station.radius, (int)Station.radius));
        fertilizerStation = new FertilizerStation(new Point((int)planeSize / 4, (int)Station.radius));
        pesticideStation = new PesticideStation(new Point((int)(2 * (planeSize / 4)) - (int)Station.radius, (int)Station.radius));

        // Set Plant targets
//        double[] xLocations =  {0.7233493299745547, 0.485279659130358, 0.971133860731706, 0.7213325750641812, 0.53096653457478,
//                0.4380008689760757, 0.14812782951389414, 0.814290355798383, 0.4013817696850509, 0.765620670676479,
//                0.80008689760757, 0.812782951389414, 0.4290355798383, 0.13817696850509, 0.5620670676479};
//        double[] yLocations = { 0.6402808236970502, 0.7357409110410246, 0.49302182625437474, 0.7685887141043787, 0.571991881125,
//                0.7240483757950078, 0.34001487550948306, 0.7387262902473831, 0.4452961950385399, 0.2573853717734149,
//                0.40483757950078, 0.001487550948306, 0.87262902473831, 0.52961950385399, 0.73853717734149};
        double[] xLocations =  {0.7233493299745547, 0.9485279659130358, 0.971133860731706, 0.7213325750641812, 0.953096653457478,
                0.4380008689760757, 0.14812782951389414, 0.814290355798383, 0.4013817696850509, 0.765620670676479};
        double[] yLocations = { 0.6402808236970502, 0.7357409110410246, 0.49302182625437474, 0.7685887141043787, 0.9571991881125,
                0.7240483757950078, 0.34001487550948306, 0.7387262902473831, 0.4452961950385399, 0.2573853717734149};

        for (int i = 0; i < xLocations.length / 2; i++) {
            nutrientPoorPlantList.add(new NutrientPoorPlant(new Point(
                    getValidPlaneCoordinate(xLocations[i], planeSize, (int)PlantTarget.radius),
                    getValidPlaneCoordinate(yLocations[i], planeSize, (int)PlantTarget.radius))));
            infestedPlantList.add(new InfestedPlant(new Point(
                    getValidPlaneCoordinate(xLocations[i + xLocations.length / 2], planeSize, (int)PlantTarget.radius),
                    getValidPlaneCoordinate(yLocations[i + xLocations.length / 2], planeSize, (int)PlantTarget.radius))));
        }
    }

    public void addLog(String logRow) {
        this.logger.addLog(logRow);
    }

    public void logToCsv() {
        this.logger.logToCsv();
    }

    public boolean fertilizePlant(Point plantPosition) {
        boolean plantFertilized = false;

        for (NutrientPoorPlant nutrientPoorPlant : nutrientPoorPlantList) {
            if (nutrientPoorPlant.getPosition().equals(plantPosition)) {
                nutrientPoorPlant.isFertilized = true;
                plantFertilized = true;
            }
        }

        return plantFertilized;
    }

    public boolean sprayPlant(Point plantPosition) {
        boolean plantSprayed = false;

        for (InfestedPlant infestedPlant : infestedPlantList) {
            if (infestedPlant.getPosition().equals(plantPosition)) {
                infestedPlant.isInfested = false;
                plantSprayed = true;
            }
        }

        return plantSprayed;
    }

    public List<NutrientPoorPlant> scanNutrientPoorPlantsInRadius(Point agentPosition, double scanRadius) {
        List<NutrientPoorPlant> nutrientPoorPlantInRadiusList = new ArrayList<NutrientPoorPlant>();
        int minX = (int)(agentPosition.x - scanRadius);
        int maxX = (int)(agentPosition.x + scanRadius);
        int minY = (int)(agentPosition.y - scanRadius);
        int maxY = (int)(agentPosition.y + scanRadius);

        for(NutrientPoorPlant plant : nutrientPoorPlantList) {
            if (plant.position.x >= minX && plant.position.x <= maxX && plant.position.y >= minY && plant.position.y <= maxY && !plant.isFertilized)
                nutrientPoorPlantInRadiusList.add(plant);
        }
        return nutrientPoorPlantInRadiusList;
    }

    public List<InfestedPlant> scanInfestedPlantsInRadius(Point agentPosition, double scanRadius) {
        List<InfestedPlant> infestedPlantInRadiusList = new ArrayList<InfestedPlant>();
        int minX = (int)(agentPosition.x - scanRadius);
        int maxX = (int)(agentPosition.x + scanRadius);
        int minY = (int)(agentPosition.y - scanRadius);
        int maxY = (int)(agentPosition.y + scanRadius);

        for(InfestedPlant plant : infestedPlantList) {
            if (plant.position.x >= minX && plant.position.x <= maxX && plant.position.y >= minY && plant.position.y <= maxY && plant.isInfested)
                infestedPlantInRadiusList.add(plant);
        }
        return infestedPlantInRadiusList;
    }

    private int getValidPlaneCoordinate(double num0to1, double planeSize, double objectRadius) {
        return (int) (num0to1 *  (planeSize - objectRadius) + objectRadius);
    }
}