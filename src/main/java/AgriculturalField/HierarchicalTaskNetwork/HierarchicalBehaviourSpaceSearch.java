package AgriculturalField.HierarchicalTaskNetwork;

final public class HierarchicalBehaviourSpaceSearch {

    // TODO: More comprehensive comments

    public void initializeAbstractionLevel() {}

    public void exchangePlans() {}

    public void removeUnConflictingPlans() {}

    public void goDeeperLevel() {}

    public void formTotalOrder() {}

    public void sendPlanToOthers() {}

    public void setSuperiorAgent() {}

    public void setPreviousSuperiorAgent() {}

    public void setInferiorAgents() {}

    public void checkPlan() {}

    public void changePlan() {}

    public void getNextAgent() {}

    public void coordinatePlans() {

        initializeAbstractionLevel(); // TODO: 1.
        exchangePlans(); // TODO: 2.
        removeUnConflictingPlans(); // TODO: 3.
        goDeeperLevel(); // TODO: 4. If deeperLevelNeed -> GOTO (2)
        resolveConflicts(); // TODO: 5.
    }

    public void resolveConflicts() {

        formTotalOrder(); // TODO: a.
        setSuperiorAgent();
        setInferiorAgents();
        sendPlanToOthers(); // TODO: b.
        checkPlan(); // TODO: c.
        changePlan();
        setPreviousSuperiorAgent(); // TODO: d.
        getNextAgent(); // TODO: If getNextAgent() == empty(), then exit() else GOTO(b)
    }
}
