package AgriculturalField.HierarchicalTaskNetwork.Planner;

import javafx.util.Pair;
import org.apache.commons.lang3.tuple.Triple;

import java.util.ArrayList;
import java.util.List;

/***
 * A compound task that contains sub-tasks.
 */
class CompoundTask extends Task {

    List<Method> methods = new ArrayList<>();

    CompoundTask(String taskName) {
        super(taskName);
    }

    Method addMethod(Triple condition) {
        Method newMethod = new Method(condition);
        methods.add(newMethod);
        return newMethod;
    }

    List<Method> methods() {
        return methods;
    }

    /**
     * Returns the first method that meets the conditions of the world state.
     * @param worldState Object world state.
     * @param startIndex Method index from the list.
     * @return Pair of method and the method list index.
     */
    Pair findSatisfiedMethod(WorldState worldState, int startIndex) {
        Method method;
        for (int i = startIndex; i < methods.size(); i++) {
            method = methods.get(i);
            if (worldState.check(method.conditions()))
                return new Pair<>(method, i);
        }
        return new Pair<Method, Integer>(null, -1);
    }
}
