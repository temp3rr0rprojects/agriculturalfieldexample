package AgriculturalField.HierarchicalTaskNetwork.Planner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/***
 * Domain contains all the methods and tasks (which can be further decomposed to other tasks).
 */
final class Domain {

    HashMap<String, Task> tasks = new HashMap<>();
    private List<Method> methods = new ArrayList<>();

    Domain() {}

    Task addTask(Task task) {
        if (tasks.containsKey(task.Name)){
            System.out.println("Warning: overriding %s with a new task" + task.Name);
        }
        tasks.put(task.Name, task);
        return task;
    }

    Task findTask(String taskName) {
        return tasks.get(taskName);
    }

    /***
     * Worker function called by 'compile' function.
     * @param task
     */
    void compileCompound(CompoundTask task) {
        methods = task.methods();
        if (methods.size() == 0) {
            System.out.println(String.format("Compound task %s has no methods", task.Name));
        } else {
          for (Method method : methods) {
              for (Task subTaskName : method.subTasks()){
                  // Check if the task is in the domain, and if not, complain
                  if (!tasks.containsKey(subTaskName.Name)) {
                      System.out.println(String.format("Task %s is using undefined task %s", task.Name, subTaskName));
                  }
              }
          }
        }
    }

    /***
     * Main entry point for domain validation. It mostly checks if all PrimitiveTasks used by CompositeTasks are defined as well.
     */
    void compile() {
        for (Object task : tasks.values()) {
            if (task instanceof CompoundTask) {
                compileCompound((CompoundTask)task);
            } else {
                continue; // TODO: Could add a check if primitive task is set up properly
            }
        }
    }

    /***
     * Nice print out of the domain
     * @return
     */
    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();

        for (Task task : tasks.values()) {
            result.append(String.format("- %s\n", task.Name));
            if (task instanceof CompoundTask) {
                for (Method method : ((CompoundTask) task).methods()) {
                    result.append(String.format("--- %s\n", method.conditions().toString()));

                    for (Task subTask : method.subTasks()) {
                        result.append(String.format("------ %s\n", subTask.toString()));
                    }
                }
            }
        }
        return result.toString();
    }
}
