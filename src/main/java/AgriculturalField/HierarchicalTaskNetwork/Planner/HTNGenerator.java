package AgriculturalField.HierarchicalTaskNetwork.Planner;

import javafx.util.Pair;
import org.apache.commons.lang3.tuple.Triple;

import java.util.Arrays;
import java.util.List;

final public class HTNGenerator {
    public HTNGenerator() {}

    Task compoundTask (Domain domain, String taskName) {
        Task task = new CompoundTask(taskName);
        domain.addTask(task);
        return task;
    }

    Task primitiveTask(Domain domain, String taskName, List<Object> conditions) {
        Task task = domain.addTask(new PrimitiveTask(taskName));
        if (conditions != null)
            ((PrimitiveTask) task).setConditions(conditions);
        return task;
    }

    public Domain getScoutUavDomain(List<String> compoundTaskNamesList) {
        Domain domain = new Domain();

        CompoundTask ct = (CompoundTask)compoundTask(domain, "Root");
        if (compoundTaskNamesList.contains("ChangeBattery")) {
            Method m1 = ct.addMethod(Triple.of("ScoutBatteryLevel", "<", 40));
            m1.addTask(new Task("ChangeBattery"));
        }
        Method m2 = ct.addMethod(Triple.of("NutrientPoorPlants", ">", 0));
        m2.addTask(new Task ("FindNutrientPoorPlantPoint"));
        m2.addTask(new Task ("BroadcastNutrientPoorPlantPoint"));
        m2.addTask(new Task ("Root"));
        Method m3 = ct.addMethod(Triple.of("InfestedPlants", ">", 0));
        m3.addTask(new Task ("FindInfestedPlantPoint"));
        m3.addTask(new Task ("BroadcastInfestedPlantPoint"));
        m3.addTask(new Task ("Root"));
        Method m4 = ct.addMethod(null);
        m4.addTask(new Task ("FindPatrolPoint"));
        m4.addTask(new Task ("NavigateToPatrolPoint"));

        // TODO: try edit existing compound task declaration
        if (compoundTaskNamesList.contains("ChangeBattery")) {
            CompoundTask ct2 = (CompoundTask) compoundTask(domain, "ChangeBattery");
            Method ma1 = ct2.addMethod(null);
            ma1.addTask(new Task("FindBattery"));
            ma1.addTask(new Task("NavigateToBattery"));
            ma1.addTask(new Task("PickUpBattery"));
            ma1.addTask(new Task("Root"));
        }

        PrimitiveTask p1 = (PrimitiveTask)primitiveTask(domain, "FindBattery", null);
        //p1.operator = new Pair<>("OpNavigateTo", "VarBatteryPickUp"); // TODO: triple?
        p1.operator = (Triple.of("OpNavigateTo", "VarBattery", "VarBatteryPickUp"));

        PrimitiveTask p2 = (PrimitiveTask)primitiveTask(domain, "NavigateToBattery", null);
        p2.operator = new Pair<>("OpNavigateTo", "VarBatteryPickUp");
        p2.effects.add(Triple.of("ScoutLocation", "=", "VarBatteryPickUp"));
        p2.effects.add(Triple.of("ScoutBatteryLevel", "+=", -20));

        PrimitiveTask p3 = (PrimitiveTask)primitiveTask(domain, "PickUpBattery", null);
        p3.operator = new Pair<>("OpPickUp", "VarBattery");
        p3.effects.add(Triple.of("ScoutBatteryLevel", "=", 100));

        PrimitiveTask p4 = (PrimitiveTask)primitiveTask(domain, "FindPatrolPoint", null);
        p4.operator = new Pair<>("OpFindPatrolPoint", "VarPatrolPoint");
        p4.effects.add(Triple.of("ScoutHasPatrolPoint", "=", true)); // TODO: true or 1?

        PrimitiveTask p5 = (PrimitiveTask)primitiveTask(domain, "FindInfestedPlantPoint", null);
        p5.operator = new Pair<>("OpFindInfestedPlantPoint", "VarInfestedPlantPoint");
        p5.effects.add(Triple.of("ScoutHasInfestedPlantPoint", "=", true));

        PrimitiveTask p6 = (PrimitiveTask)primitiveTask(domain, "FindNutrientPoorPlantPoint", null);
        p6.operator = new Pair<>("OpFindNutrientPoorPlantPoint", "VarNutrientPoorPlantPoint");
        p6.effects.add(Triple.of("ScoutHasNutrientPoorPlantPoint", "=", true));

        PrimitiveTask p7 = (PrimitiveTask)primitiveTask(domain, "NavigateToPatrolPoint", null);
        p7.operator = new Pair<>("OpNavigateTo", "VarPatrolPoint");
        p7.effects.add(Triple.of("ScoutLocation", "=", "VarPatrolPoint"));
        p7.effects.add(Triple.of("ScoutBatteryLevel", "+=", -20));

        PrimitiveTask p8 = (PrimitiveTask)primitiveTask(domain, "BroadcastInfestedPlantPoint", null);
        p8.operator = new Pair<>("OpBroadcast", "VarInfestedPlantPoint");
        p8.effects.add(Triple.of("InfestedPlantLocation", "=", "VarInfestedPlantPoint"));
        p8.effects.add(Triple.of("ScoutBatteryLevel", "+=", -10));
        p8.effects.add(Triple.of("InfestedPlants", "+=", -1));

        PrimitiveTask p9 = (PrimitiveTask)primitiveTask(domain, "BroadcastNutrientPoorPlantPoint", null);
        p9.operator = new Pair<>("OpBroadcast", "VarNutrientPoorPlantPoint");
        p9.effects.add(Triple.of("NutrientPoorPlantLocation", "=", "VarNutrientPoorPlantPoint"));
        p9.effects.add(Triple.of("ScoutBatteryLevel", "+=", -10));
        p9.effects.add(Triple.of("NutrientPoorPlants", "+=", -1));

        domain.compile();
        return domain;
    }

    public WorldState getInitScoutUavWorldState() {
        Domain domain = getScoutUavDomain(Arrays.asList("ChangeBattery"));

        WorldState worldState = new WorldState(domain);
        worldState.knowledge.put("InfestedPlants", 0);
        worldState.knowledge.put("NutrientPoorPlants", 0);
        worldState.knowledge.put("ScoutBatteryLevel", 100);
        return worldState;
    }

    public Domain getBomberUavDomain(List<String> compoundTaskNamesList) {
        Domain domain = new Domain();

        CompoundTask ct = (CompoundTask) compoundTask(domain, "Root");
        if (compoundTaskNamesList.contains("ChangeBattery")) {
            Method m1 = ct.addMethod(Triple.of("BomberBatteryLevel", "<", 40));
            m1.addTask(new Task("ChangeBattery"));
        }
        if (compoundTaskNamesList.contains("SprayInfestedPlant")) {
            Method m3 = ct.addMethod(Triple.of("InfestedPlants", ">", 0));
            m3.addTask(new Task("SprayInfestedPlant"));
        }
        Method m4 = ct.addMethod(null);
        m4.addTask(new Task("FindPatrolPoint"));
        m4.addTask(new Task("NavigateToPatrolPoint"));

        if (compoundTaskNamesList.contains("ChangeBattery")) {
            CompoundTask ct2 = (CompoundTask) compoundTask(domain, "ChangeBattery");
            Method ma1 = ct2.addMethod(null);
            ma1.addTask(new Task("FindBattery"));
            ma1.addTask(new Task("NavigateToBattery"));
            ma1.addTask(new Task("PickUpBattery"));
            ma1.addTask(new Task("Root"));
        }

        if (compoundTaskNamesList.contains("SprayInfestedPlant")) {
            CompoundTask ct3 = (CompoundTask) compoundTask(domain, "SprayInfestedPlant");
            Method mb1 = ct3.addMethod(Triple.of("BomberPesticideContainerLevel", ">", 0));
            mb1.addTask(new Task("NavigateToInfestedPlant"));
            mb1.addTask(new Task("UsePesticide"));
            mb1.addTask(new Task("Root"));
            Method mb2 = ct3.addMethod(null);
            mb2.addTask(new Task("FindPesticide"));
            mb2.addTask(new Task("NavigateToPesticide"));
            mb2.addTask(new Task("PickUpPesticide"));
            mb2.addTask(new Task("SprayInfestedPlant"));
        }

        PrimitiveTask p1 = (PrimitiveTask)primitiveTask(domain, "FindBattery", null);
        //p1.operator = new Pair<>("OpNavigateTo", "VarBatteryPickUp"); // TODO: triple?
        p1.operator = (Triple.of("OpNavigateTo", "VarBattery", "VarBatteryPickUp"));

        PrimitiveTask p2 = (PrimitiveTask)primitiveTask(domain, "NavigateToBattery", null);
        p2.operator = new Pair<>("OpNavigateTo", "VarBatteryPickUp");
        p2.effects.add(Triple.of("BomberLocation", "=", "VarBatteryPickUp"));
        p2.effects.add(Triple.of("BomberBatteryLevel", "+=", -20));

        PrimitiveTask p3 = (PrimitiveTask)primitiveTask(domain, "PickUpBattery", null);
        p3.operator = new Pair<>("OpPickUp", "VarBattery");
        p3.effects.add(Triple.of("BomberBatteryLevel", "=", 100));

        PrimitiveTask p4 = (PrimitiveTask)primitiveTask(domain, "FindPatrolPoint", null);
        p4.operator = new Pair<>("OpFindPatrolPoint", "VarPatrolPoint");
        p4.effects.add(Triple.of("BomberHasPatrolPoint", "=", true)); // TODO: true or 1?

        PrimitiveTask p5 = (PrimitiveTask)primitiveTask(domain, "FindPesticide", null);
        p5.operator = Triple.of("OpFindPesticide", "VarPesticide", "VarPesticidePickUp");

        PrimitiveTask p7 = (PrimitiveTask)primitiveTask(domain, "NavigateToPatrolPoint", null);
        p7.operator = new Pair<>("OpNavigateTo", "VarPatrolPoint");
        p7.effects.add(Triple.of("BomberLocation", "=", "VarPatrolPoint"));
        p7.effects.add(Triple.of("BomberBatteryLevel", "+=", -20));

        PrimitiveTask p8 = (PrimitiveTask)primitiveTask(domain, "NavigateToInfestedPlant", null);
        p8.operator = new Pair<>("OpNavigateTo", "BomberInfestedPlant");
        p8.effects.add(Triple.of("BomberLocation", "=", "BomberInfestedPlant"));
        p8.effects.add(Triple.of("BomberCanSeeInfestedPlant", "=", true));
        p8.effects.add(Triple.of("BomberBatteryLevel", "+=", -10));

        PrimitiveTask p9 = (PrimitiveTask)primitiveTask(domain, "NavigateToPesticide", null);
        p9.operator = new Pair<>("OpNavigateTo", "VarPesticidePickUp");
        p9.effects.add(Triple.of("BomberLocation", "=", "VarPesticidePickUp"));
        p9.effects.add(Triple.of("BomberBatteryLevel", "+=", -20));

        PrimitiveTask p10 = (PrimitiveTask)primitiveTask(domain, "UsePesticide", null);
        p10.operator = new Pair<>("OpUsePesticide", "BomberInfestedPlant");
        p10.effects.add(Triple.of("BomberPesticideContainerLevel", "+=", -1));
        p10.effects.add(Triple.of("InfestedPlants", "+=", -1));
        p10.effects.add(Triple.of("BomberBatteryLevel", "+=", -10));

        PrimitiveTask p11 = (PrimitiveTask)primitiveTask(domain, "PickUpPesticide", null);
        p11.operator = new Pair<>("OpPickUp", "VarPesticide");
        p11.effects.add(Triple.of("BomberPesticideContainerLevel", "=", 2));

        domain.compile();
        return domain;
    }

    public WorldState getInitBomberUavWorldState() {
        Domain domain = getBomberUavDomain(Arrays.asList("ChangeBattery"));

        WorldState worldState = new WorldState(domain);
        worldState.knowledge.put("BomberPesticideContainerLevel", 0);
        worldState.knowledge.put("InfestedPlants", 0);
        worldState.knowledge.put("BomberBatteryLevel", 100);
        return worldState;
    }

    public Domain getTankAgvDomain(List<String> compoundTaskNamesList) {
        Domain domain = new Domain();

        CompoundTask ct = (CompoundTask)compoundTask(domain, "Root");
        if (compoundTaskNamesList.contains("ChangeBattery")) {
            Method m1 = ct.addMethod(Triple.of("TankBatteryLevel", "<", 40));
            m1.addTask(new Task("ChangeBattery"));
        }
        if (compoundTaskNamesList.contains("FertilizeNutrientPoorPlant")) {
            Method m3 = ct.addMethod(Triple.of("NutrientPoorPlants", ">", 0));
            m3.addTask(new Task("FertilizeNutrientPoorPlant"));
        }
        Method m4 = ct.addMethod(null);
        m4.addTask(new Task ("FindPatrolPoint"));
        m4.addTask(new Task ("NavigateToPatrolPoint"));

        if (compoundTaskNamesList.contains("ChangeBattery")) {
            CompoundTask ct2 = (CompoundTask) compoundTask(domain, "ChangeBattery");
            Method ma1 = ct2.addMethod(null); // TODO: test condition
            ma1.addTask(new Task("FindBattery"));
            ma1.addTask(new Task("NavigateToBattery"));
            ma1.addTask(new Task("PickUpBattery"));
            ma1.addTask(new Task("Root"));
        }

        if (compoundTaskNamesList.contains("FertilizeNutrientPoorPlant")) {
            CompoundTask ct3 = (CompoundTask) compoundTask(domain, "FertilizeNutrientPoorPlant");
            Method mb1 = ct3.addMethod(Triple.of("TankFertilizerContainerLevel", ">", 0));
            mb1.addTask(new Task("NavigateToNutrientPoorPlant"));
            mb1.addTask(new Task("UseFertilizer"));
            mb1.addTask(new Task("Root"));
            Method mb2 = ct3.addMethod(null);
            mb2.addTask(new Task("FindFertilizer"));
            mb2.addTask(new Task("NavigateToFertilizer"));
            mb2.addTask(new Task("PickUpFertilizer"));
            mb2.addTask(new Task("FertilizeNutrientPoorPlant"));
        }

        PrimitiveTask p1 = (PrimitiveTask)primitiveTask(domain, "FindBattery", null);
        //p1.operator = new Pair<>("OpNavigateTo", "VarBatteryPickUp"); // TODO: triple?
        p1.operator = (Triple.of("OpNavigateTo", "VarBattery", "VarBatteryPickUp"));

        PrimitiveTask p2 = (PrimitiveTask)primitiveTask(domain, "NavigateToBattery", null);
        p2.operator = new Pair<>("OpNavigateTo", "VarBatteryPickUp");
        p2.effects.add(Triple.of("TankLocation", "=", "VarBatteryPickUp"));
        p2.effects.add(Triple.of("TankBatteryLevel", "+=", -10));

        PrimitiveTask p3 = (PrimitiveTask)primitiveTask(domain, "PickUpBattery", null);
        p3.operator = new Pair<>("OpPickUp", "VarBattery");
        p3.effects.add(Triple.of("TankBatteryLevel", "=", 100));

        PrimitiveTask p4 = (PrimitiveTask)primitiveTask(domain, "FindPatrolPoint", null);
        p4.operator = new Pair<>("OpFindPatrolPoint", "VarPatrolPoint");
        p4.effects.add(Triple.of("TankHasPatrolPoint", "=", true)); // TODO: true or 1?

        PrimitiveTask p5 = (PrimitiveTask)primitiveTask(domain, "FindFertilizer", null);
        p5.operator = Triple.of("OpFindFertilizer", "VarFertilizer", "VarFertilizerPickUp");

        PrimitiveTask p7 = (PrimitiveTask)primitiveTask(domain, "NavigateToPatrolPoint", null);
        p7.operator = new Pair<>("OpNavigateTo", "VarPatrolPoint");
        p7.effects.add(Triple.of("TankLocation", "=", "VarPatrolPoint"));
        p7.effects.add(Triple.of("TankBatteryLevel", "+=", -10));

        PrimitiveTask p8 = (PrimitiveTask)primitiveTask(domain, "NavigateToNutrientPoorPlant", null);
        p8.operator = new Pair<>("OpNavigateTo", "TankNutrientPoorPlant");
        p8.effects.add(Triple.of("TankLocation", "=", "TankNutrientPoorPlant"));
        p8.effects.add(Triple.of("TankCanSeeNutrientPoorPlant", "=", true));
        p8.effects.add(Triple.of("TankBatteryLevel", "+=", -10));

        PrimitiveTask p9 = (PrimitiveTask)primitiveTask(domain, "NavigateToFertilizer", null);
        p9.operator = new Pair<>("OpNavigateTo", "VarPesticidePickUp");
        p9.effects.add(Triple.of("TankLocation", "=", "VarFertilizerPickUp"));
        p9.effects.add(Triple.of("TankBatteryLevel", "+=", -10));

        PrimitiveTask p10 = (PrimitiveTask)primitiveTask(domain, "UseFertilizer", null);
        p10.operator = new Pair<>("OpUseFertilizer", "TankNutrientPoorPlant");
        p10.effects.add(Triple.of("TankFertilizerContainerLevel", "+=", -1));
        p10.effects.add(Triple.of("NutrientPoorPlants", "+=", -1));
        p10.effects.add(Triple.of("TankBatteryLevel", "+=", -10));

        PrimitiveTask p11 = (PrimitiveTask)primitiveTask(domain, "PickUpFertilizer", null);
        p11.operator = new Pair<>("OpPickUp", "VarFertilizer");
        p11.effects.add(Triple.of("TankFertilizerContainerLevel", "=", 4));

        domain.compile();
        return domain;
    }

    public WorldState getInitTankAgvWorldState() {
        Domain domain = getTankAgvDomain(Arrays.asList("ChangeBattery"));

        WorldState worldState = new WorldState(domain);
        worldState.knowledge.put("TankFertilizerContainerLevel", 0);
        worldState.knowledge.put("NutrientPoorPlants", 0);
        worldState.knowledge.put("TankBatteryLevel", 100);
        return worldState;
    }

}
