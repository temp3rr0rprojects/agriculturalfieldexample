package AgriculturalField.HierarchicalTaskNetwork.Planner;

import com.google.common.collect.Lists;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/***
 * The Planner that attempts to find a valid plan through the compound tasks of the Domain
 * and WorldState.
 */
final public class HTNPlanner {
    Domain domain;
    WorldState startWorldState;
    Stack<List<Object>> rollBackStack = new Stack<>();
    public List<Object> abstractFinalPlan;
    public Object finalPlan;
    public WorldState finalWorldState;

    public HTNPlanner(Domain domain, WorldState worldState) {
        this.domain = domain;
        this.startWorldState = worldState;
    }

    /***
     * Records a store point of the current plan status.
     * @param currentTask
     * @param methodIndex
     * @param workingWs
     * @param finalPlan
     */
    void recordDecomposition(Task currentTask, int methodIndex, WorldState workingWs, Object finalPlan) {
        List<Object> item = new ArrayList<>();
        item.add(currentTask);
        item.add(methodIndex);
        item.add(workingWs);
        item.add(finalPlan);
        rollBackStack.push(item);
    }

    /***
     * Plan backtrack of one step, using the roll back stack.
     * @return
     */
    List<Object> restoreLastDecomposition() {
        return rollBackStack.pop();
    }

    void printProgress (List<Object> finalPlan, Stack<String> tasksToProcess){
        System.out.println(finalPlan);
        System.out.println(tasksToProcess);
    }

    /***
     * THe main plan generation execution.
     * @param taskName
     * @return
     */
    public Object generatePlan(String taskName) { // TODO: List<String>? TODO: store higher level abstraction paths too?
        List<Object> finalPlan = new ArrayList<>();
        abstractFinalPlan = new ArrayList<>();
        rollBackStack = new Stack<>();
        WorldState workingWorldState = startWorldState;
        Stack<String> tasksToProcess = new Stack<>();
        tasksToProcess.add(taskName);
        String currentTaskName;
        Task currentTask;
        int next_method;
        while (tasksToProcess.size() > 0) {
//            printProgress(finalPlan, tasksToProcess);
            currentTaskName = tasksToProcess.pop();
            currentTask = domain.findTask(currentTaskName);
            next_method = 0;

            if (currentTask instanceof CompoundTask) {
                Pair<Method, Integer> satisfiedMethodPair = ((CompoundTask) currentTask).findSatisfiedMethod(workingWorldState, next_method);
                Method satisfiedMethod = satisfiedMethodPair.getKey();
                int methodIndex = satisfiedMethodPair.getValue();
                if (satisfiedMethod != null) {
                    recordDecomposition(currentTask, methodIndex, workingWorldState, finalPlan);
                    abstractFinalPlan.add(currentTask.Name); // Store to top-level abstract plan view
                    List<Task> tmpTasks = satisfiedMethodPair.getKey().subTasks();
                    tmpTasks = Lists.reverse(tmpTasks);

                    for (Task task : tmpTasks) // Extend stack with sub-tasks {
                        tasksToProcess.push(task.Name);
                } else {
                    // current_task, next_method, working_ws, final_plan = self.restore_last_decomposition()
                    List<Object> lastDecomposition = restoreLastDecomposition();
                    currentTask = (Task)lastDecomposition.get(0);
                    next_method = (int)lastDecomposition.get(1);
                    workingWorldState = (WorldState)lastDecomposition.get(2);
                    finalPlan = (List<Object>)lastDecomposition.get(3);
                    tasksToProcess.push(currentTask.Name);
                }
            } else {
                if (((PrimitiveTask) currentTask).checkCondition(workingWorldState)){
                    workingWorldState.apply(((PrimitiveTask) currentTask).effects);
                    finalPlan.add(currentTaskName);
                } else {
                    List<Object> lastDecomposition = restoreLastDecomposition();
                    currentTask = (Task)lastDecomposition.get(0);
                    next_method = (int)lastDecomposition.get(1);
                    workingWorldState = (WorldState)lastDecomposition.get(2);
                    finalPlan = (List<Object>)lastDecomposition.get(3);
                    tasksToProcess.push(currentTask.Name);
                }
            }
        }

        this.finalPlan = finalPlan;
        this.finalWorldState = workingWorldState;

        return finalPlan;
    }
}
