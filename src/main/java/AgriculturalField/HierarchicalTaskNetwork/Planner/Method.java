package AgriculturalField.HierarchicalTaskNetwork.Planner;

import org.apache.commons.lang3.tuple.Triple;

import java.util.ArrayList;
import java.util.List;

/***
 * Helps to decompose CompoundTasks into sub-tasks.
 */
final class Method {
    List<Task> subtasks = new ArrayList<>();
    List<Triple> conditions = new ArrayList<>();

    Method(Triple conditions) {
        this.conditions.add(conditions);
    }

    void addTask(Task task) {
        subtasks.add(task);
    }

    List<Triple> conditions() {
        return conditions;
    }

    List<Task> subTasks() {
        return subtasks;
    }
}
