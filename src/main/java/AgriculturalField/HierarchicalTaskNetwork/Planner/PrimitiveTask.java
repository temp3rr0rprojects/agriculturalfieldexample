package AgriculturalField.HierarchicalTaskNetwork.Planner;

import org.apache.commons.lang3.tuple.Triple;

import java.util.ArrayList;
import java.util.List;

/***
 * Primitive tasks can't be decomposed any more. Conditions, effects and operators
 * express its parameters.
 */
final class PrimitiveTask extends Task {
    List<Triple> effects = new ArrayList<>();
    private List<Object> conditions = new ArrayList<>();
    Object operator;

    PrimitiveTask(String taskName) {
        super(taskName);
    }

    boolean checkCondition(WorldState worldState) {
        return worldState.check(conditions);
    }

    List<Object> conditions() {
        return conditions;
    }

    void setConditions(List<Object> conditions) {
        this.conditions = conditions;
    }
}
