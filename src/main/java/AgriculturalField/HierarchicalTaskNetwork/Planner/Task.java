package AgriculturalField.HierarchicalTaskNetwork.Planner;

/***
 * A typical task, base class.
 */
class Task {
    String Name;
    Task(String name) {
        Name = name;
    }
}