package AgriculturalField.HierarchicalTaskNetwork.Planner;

import org.apache.commons.lang3.tuple.Triple;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/***
 * Knowledge about the world.
 */
final public class WorldState {

    public HashMap knowledge; // key-value representation of world knowledge
    HashMap<String, Object> ops; // Operation lookup

    WorldState(Domain domain) {

        knowledge = new HashMap(); // key-value representation of world knowledge

        // operation look-up
        ops = new HashMap<>();
        ops.put("==", null);
        ops.put("=", null);
        ops.put(">", null);
        ops.put("+=", null);
        ops.put("<", null);

        // If domain has been passed in we can use it to seed the world state knowledge dictionary with expected keys
        if (domain != null) {
            for (Task task : domain.tasks.values()) {
                if (task instanceof CompoundTask) {
                    for (Method method : ((CompoundTask) task).methods)
                        if (method.conditions().get(0) != null)
                            knowledge.put(method.conditions().get(0).getLeft(), null);
                } else {
                    for (Object key : (((PrimitiveTask) task).conditions()))
                        knowledge.put(((Triple)key).getLeft(), null);
                    for (Object key : (((PrimitiveTask) task).effects))
                        knowledge.put(((Triple)key).getLeft(), null);
                }
            }
        }
    }

    /***
     * For the list-like reading. TODO: need?
     * @param key
     * @return
     */
    Object getItem(int key) {
        return knowledge.get(key);
    }

    /***
     * For the list-like assigning. TODO: need?
     * @param key
     * @param value
     */
    void setItem(int key, Object value) {
        knowledge.put(key, value);
    }

    @Override
    public String toString() {
        String result = "";
        result = knowledge.toString();

        return result;
    }
    /***
     * Applies given 'effect' to the stored world knowledge
     * @param effect
     */
    void apply(List<Triple> effect) {
        if (effect.size() == 1) {
            String key = (String)effect.get(0).getLeft();
            String operation = (String)effect.get(0).getMiddle();

            if (knowledge.containsKey(key) && ops.containsKey(operation)) {
                Object argumentIn = effect.get(0).getRight();
                int argument = 0;
                if (argumentIn instanceof Integer)
                    argument = (int)effect.get(0).getRight();
                else if (argumentIn instanceof String)
                    if (knowledge.containsKey((String)argumentIn))
                        argument = (int)knowledge.get((String)argumentIn);

                int operationResult;

                switch (operation){
                    case "==":
                        boolean operationResultBoolean = ((int)knowledge.get(key) == argument);
                        knowledge.put(key, operationResultBoolean ? 1 : 0);
                        break;
                    case ">":
                        operationResultBoolean = ((int)knowledge.get(key) > argument);
                        knowledge.put(key, operationResultBoolean ? 1 : 0);
                        break;
                    case "<":
                        operationResultBoolean = ((int)knowledge.get(key) < argument);
                        knowledge.put(key, operationResultBoolean ? 1 : 0);
                        break;
                    case "=":
                        operationResult = (int)argument;
                        knowledge.put(key, operationResult);
                        break;
                    case "+=":
                        operationResult = (int)knowledge.get(key) + (int)argument;
                        knowledge.put(key, operationResult);
                        break;
                }
            }
        } else {
            for (Triple item : effect) {
                List<Triple> tripleItem = new ArrayList<Triple>();
                tripleItem.add(item);
                apply(tripleItem);
            }
        }
    }

    /***
     * Check the condition against the stored world knowledge.
     * @param condition
     * @return
     */
    boolean check(Object condition) {
        if (condition == null) {
            return true;
        } else if (!(condition instanceof List)){

            String key = (String)((Triple)condition).getLeft();
            String operation = (String)((Triple) condition).getMiddle();

            if (knowledge.containsKey(key) && ops.containsKey(operation)) {

                Object argumentIn = ((Triple) condition).getRight();
                int argument = 0;

                if (argumentIn instanceof Integer)
                    argument = (int)argumentIn;
                else if (argumentIn instanceof String)
                    if (knowledge.containsKey((String)argumentIn))
                        argument = (int)knowledge.get((String)argumentIn);

                boolean operationResultBoolean = false;

                switch (operation) {
                    case "==":
                        operationResultBoolean = ((int) knowledge.get(key) == argument);
                        break;
                    case ">":
                        operationResultBoolean = ((int) knowledge.get(key) > argument);
                        break;
                    case "<":
                        operationResultBoolean = ((int) knowledge.get(key) < argument);
                        break;
                    case "=": // TODO: return true?
                        //operationResult = (int)argument;
                        operationResultBoolean = true;
                        break;
                    case "+=": // TODO: return true?
                        //operationResult = (int)knowledge.get(key) + (int)argument;
                        operationResultBoolean = true;
                        break;
                }

                return  operationResultBoolean;
            } else {
                return false;
            }
        } else {
            for (Triple item : (List<Triple>)condition)
                if (!check(item))
                    return false;
            return true;
        }
    }
}
